<div class="blog-card card-body-animated">
    <div class="card shadow news-animation">
        <a target="_blank"
            href="https://miit.uz/ru/news/ozbekiston-buyuk-britaniya-hamkorligini-kengaytirish-istiqbollari">
            <img src="./Главная _ Task Manager_files/39b9f04a64634869cfc4bb5132bf6540.jpg"
                class="card-img-top rounded-top news-item__img q-img__image q-img__image--with-transition q-img__image--loaded"
                alt="Обсуждены перспективы расширения узбекско-британского партнерства"
                style="object-fit: cover; object-position: 50% 50%;">
        </a>
        <div class="card-body">
            <small class="d-block mb-2">2023-01-20</small>
            <h2 class="h5">
                <a target="_blank"
                    href="https://miit.uz/ru/news/ozbekiston-buyuk-britaniya-hamkorligini-kengaytirish-istiqbollari">Обсуждены
                    перспективы расширения узбекско-британского
                    партнерства</a>
            </h2>
        </div>
    </div>
</div>