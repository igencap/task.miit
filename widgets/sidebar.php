<a class="sidebar-toggle-btn show" href="#">
    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M21.6777 20.1102C21.8842 20.3193 22 20.6018 22 20.8962C22 21.1905 21.8842 21.473 21.6777 21.6821C21.4676 21.886 21.1867 22 20.8944 22C20.6021 22 20.3213 21.886 20.1111 21.6821L11 12.526L1.88888 21.6821C1.67874 21.886 1.39787 22 1.10557 22C0.81326 22 0.532392 21.886 0.322256 21.6821C0.115813 21.473 0 21.1905 0 20.8962C0 20.6018 0.115813 20.3193 0.322256 20.1102L9.44712 10.9678L0.322256 1.82553C0.146946 1.6112 0.057355 1.33909 0.0709105 1.06214C0.0844659 0.785197 0.200198 0.523211 0.395597 0.327143C0.590995 0.131076 0.852088 0.0149475 1.12809 0.00134567C1.40409 -0.0122562 1.67527 0.0776412 1.88888 0.253552L11 9.40966L20.1111 0.253552C20.3247 0.0776412 20.5959 -0.0122562 20.8719 0.00134567C21.1479 0.0149475 21.409 0.131076 21.6044 0.327143C21.7998 0.523211 21.9155 0.785197 21.9291 1.06214C21.9426 1.33909 21.8531 1.6112 21.6777 1.82553L12.5529 10.9678L21.6777 20.1102Z"
            fill="white" />
    </svg>

</a>

<a class="sidebar-brand brand-logo" href="https://task.miit.uz/"><img class="dashboard-image"
        src="./Главная _ Task Manager_files/logo-white-text.png"></a>
<div class="sidebar-scroll ps" style="height: 100%; position: relative; background-color: #286196;">

    <ul id="sidebar-menu d-flex flex-column justify-content-center align-items-center" class="sidebar-menu">
        <li class="active    main">
            <a href="https://task.miit.uz/index.php/dashboard">
                <span class="icon icon-monitor"></span>
                <span class="menu-text ">Главная</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://taskdev.miit.uz/index.php/presence">
                <span class="icon icon-user-check"></span>
                <span class="menu-text ">Локации руководителей</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/events">
                <span class="icon icon-calendar"></span>
                <span class="menu-text ">Календарь</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/tickets">
                <span class="icon icon-gov-program"></span>
                <span class="menu-text ">Гос. Программа МИИТ</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/projects/all_projects">
                <span class="icon icon-grid"></span>
                <span class="menu-text ">Проекты</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/projects/all_tasks">
                <span class="icon icon-check-circle"></span>
                <span class="menu-text ">Задачи</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/notes">
                <span class="icon icon-book"></span>
                <span class="menu-text ">Заметки</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://taskdev.miit.uz/index.php/gifts">
                <span class="icon icon-award"></span>
                <span class="menu-text ">Подарки</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/messages">
                <span class="icon icon-message-circle"></span>
                <span class="menu-text ">Сообщения</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://taskdev.miit.uz/index.php/team_members">
                <span class="icon icon-team"></span>
                <span class="menu-text ">Команда</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://task.miit.uz/index.php/tickets">
                <span class="icon icon-suport"></span>
                <span class="menu-text ">Служба поддержки</span>
            </a>
        </li>

        <li class="    main">
            <a href="https://taskdev.miit.uz/index.php/docs/dashboard">
                <span class="icon icon-file"></span>
                <span class="menu-text ">Документы</span>
            </a>
        </li>

        <li class="  expand    main">
            <a href="#">
                <span class="icon icon-pie-chart"></span>
                <span class="menu-text ">Система</span>
            </a>
            <ul>
                <li>
                    <a href="https://taskdev.miit.uz/index.php/team">
                        <span class="icon icon-team"></span>
                        <span>Команда</span>
                    </a>
                </li>
                <li>
                    <a href="https://taskdev.miit.uz/index.php/department">
                        <span class="icon icon-management"></span>
                        <span>Управления</span>
                    </a>
                </li>
                <li>
                    <a href="https://taskdev.miit.uz/index.php/direction">
                        <span class="icon icon-direction"></span>
                        <span>Направление</span>
                    </a>
                </li>
                <li>
                    <a href="https://taskdev.miit.uz/index.php/ldap">
                        <span class="icon icon-user"></span>
                        <span>Пользователи</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="    main">
            <a href="https://taskdev.miit.uz/index.php/settings/general">
                <span class="icon icon-settings"></span>
                <span class="menu-text ">Настройки</span>
            </a>
        </li>
    </ul>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
    </div>
</div>