<div class="widget-container col-xl-12 col-sm-12">
    <a href="https://task.miit.uz/index.php/tickets" class="white-link">
        <div class="card dashboard-icon-widget">
            <div class="card-body card-body-animated">
                <div class="widget-icon bg-transparent">
                    <img src="./Главная _ Task Manager_files/service-10.svg" width="70">
                </div>
                <div class="widget-details">
                    <b class="text-default">Служба поддержки</b>

                    <div class="widget-details-inner">
                        <span>В процессе: 45</span>
                        <span>Выполненные: 50</span>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>