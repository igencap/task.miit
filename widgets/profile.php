<div class="box mt-10" style="margin-top:10px;">
    <div class="box-content text-end">
        <p class="p0 m0">
            <label class="badge bg-info large p10"><strong> Управление ИКТ
                </strong></label><br>
        </p>
    </div>
</div>


<div class="col-xl-12 col-12">
    <div class="row p20">
        <style type="text/css">
            .avatar.avatar-lg {
                display: block;
            }

            .file-upload {
                display: none;
            }

            .badge.bg-info.large {
                white-space: normal;
                line-height: 1.2;
            }
        </style>
        <div class="box d-flex justify-content-center" id="profile-image-section">

            <div class="box-content text-center profile-image">
                <form action="https://task.miit.uz/index.php/team_members/save_profile_image/8" id="profile-image-form"
                    class="general-form" role="form" method="post" accept-charset="utf-8">
                    <input type="hidden" name="rise_csrf_token" value="4c51843ed8bdedf29a60b7aecec0e323"> <a
                        href="https://task.miit.uz/index.php/team_members/view/8/general">

                        <div class="file-upload btn p0 profile-image-upload profile-image-direct-upload"
                            data-bs-toggle="tooltip" data-placement="right" aria-label="Загрузить (200x200 px)">
                            <input type="file" name="profile_image_file" id="profile_image_file_upload"
                                class="no-outline hidden-input-file upload">
                        </div>
                        <input type="hidden" id="profile_image" name="profile_image" value="">
                        <span class="avatar avatar-lg">
                            <img id="profile-image-preview"
                                src="./Главная _ Task Manager_files/_file62c5272dcc735-avatar.png" alt="...">
                        </span>
                        <h4 class="profile-name">Анвар Рахимов</h4>
                        <p class="profile-staff">
                            Главный специалист
                        </p>
                    </a>
                </form>
            </div>
        </div>
    </div>


    <style type="text/css">
        .left-menu-item .p10.m0.text-left {
            text-align: center;
        }
    </style>
    <script>
        $(document).ready(function () {
            //modify design for mobile devices
            if (isMobile()) {
                $("#profile-image-section").children("div").each(function () {
                    $(this).addClass("p0");
                    $(this).removeClass("box-content");
                });
            }

            $('[data-bs-toggle="tooltip"]').tooltip();
        });
    </script>
</div>