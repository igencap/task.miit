<div class="card bg-white">
    <div class="card-header">
        <span>Дни рождения</span>
        <a href="#">Все</a>
    </div>
    <div class="card-body">
        <div class="d-flex align-items-center border-bottom mb-3">
            <div class="flex-shrink-0">
                <span class="avatar avatar-xs">
                    <img src="./Главная _ Task Manager_files/_file62bafd73733e6-avatar.png" alt="Алижон  Абдуллаев ">
                </span>
            </div>
            <div class="p-2 w-100">
                <div class="card-title d-flex flex-column justify-content-center">
                    <a href="https://task.miit.uz/index.php/team_members/view/64" class="dark strong">Алижон Абдуллаев
                    </a>
                    <span class="text-info">20-01-1974</span>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center border-bottom mb-3">
            <div class="flex-shrink-0">
                <span class="avatar avatar-xs">
                    <img src="./Главная _ Task Manager_files/_file62aab84bc64c8-avatar.png" alt="Мурад  Сулейманов">
                </span>
            </div>
            <div class="p-2 w-100">
                <div class="card-title d-flex flex-column justify-content-center"">
                                <a href=" https://task.miit.uz/index.php/team_members/view/4" class="dark strong">Мурад
                    Сулейманов</a>
                    <span class="text-info">24-01-1998</span>
                </div>
            </div>
        </div>
    </div>
</div>