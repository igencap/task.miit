<script>
    am5.ready(function () {

        // Create root element
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("graph");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/radar-chart/
        var chart = root.container.children.push(am5radar.RadarChart.new(root, {
            panX: false,
            panY: false,
            innerRadius: am5.percent(20),
            startAngle: 0,
            endAngle: 360
        }));


        // Data
        var data = [{
            category: "",
            value: 0,
            full: 0,
            columnSettings: {
                fill: chart.get("colors").getIndex(0)
            }
        }, {
            category: "1",
            value: 31,
            full: 50,
            columnSettings: {
                fill: chart.get("colors").getIndex(2)
            }
        }, {
            category: "",
            value: 31,
            full: 50,
            columnSettings: {
                fill: chart.get("colors").getIndex(15)
            }
        }, {
            category: "",
            value: 0,
            full: 0,
            columnSettings: {
                fill: chart.get("colors").getIndex(3)
            }
        }];

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Cursor
        var cursor = chart.set("cursor", am5radar.RadarCursor.new(root, {
            behavior: "zoomX"
        }));

        cursor.lineY.set("visible", false);

        // Create axes and their renderers
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_axes
        var xRenderer = am5radar.AxisRendererCircular.new(root, {
            //minGridDistance: 50
        });

        xRenderer.labels.template.setAll({
            radius: 5000
        });

        xRenderer.grid.template.setAll({
            forceHidden: true
        });

        var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
            renderer: xRenderer,
            min: 0,
            max: 50,
            strictMinMax: true,
            numberFormat: "#'%'",
            tooltip: am5.Tooltip.new(root, {})
        }));


        var yRenderer = am5radar.AxisRendererRadial.new(root, {
            minGridDistance: 20
        });

        yRenderer.labels.template.setAll({
            centerX: am5.p100,
            fontWeight: "500",
            fontSize: 18,
            templateField: "columnSettings"
        });

        yRenderer.grid.template.setAll({
            forceHidden: true
        });

        var yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
            categoryField: "category",
            renderer: yRenderer
        }));

        yAxis.data.setAll(data);


        // Create series
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_series
        var series1 = chart.series.push(am5radar.RadarColumnSeries.new(root, {
            xAxis: xAxis,
            yAxis: yAxis,
            clustered: false,
            valueXField: "full",
            categoryYField: "category",
            fill: root.interfaceColors.get("alternativeBackground")
        }));

        series1.columns.template.setAll({
            width: am5.p100,
            fillOpacity: 0.08,
            strokeOpacity: 0,
            cornerRadius: 5000
        });

        series1.data.setAll(data);


        var series2 = chart.series.push(am5radar.RadarColumnSeries.new(root, {
            xAxis: xAxis,
            yAxis: yAxis,
            clustered: false,
            valueXField: "value",
            categoryYField: "category"
        }));

        series2.columns.template.setAll({
            width: am5.p100,
            strokeOpacity: 0,
            tooltipText: "{category}: {valueX}%",
            cornerRadius: 5000,
            templateField: "columnSettings"
        });

        series2.data.setAll(data);

        // Animate chart and series in
        // https://www.amcharts.com/docs/v5/concepts/animations/#Initial_animation
        series1.appear(1000);
        series2.appear(1000);
        chart.appear(1000, 100);

    }); // end am5.ready()
</script>

<div class="graph-title">
    Экспорт товаров/услуг
</div>

<div class="" id="graph"></div>

<div class="graph-info">
    <div class="graph-info-item">
        <span class="graph-info-number graph-info--color1">
            443709 / 7362744
        </span>

        <span class="graph-info-text graph-info--purple">
            <div class="graph-info-square graph-info-square-color1"></div>
            Промышленность в тыс. $
        </span>
    </div>

    <div class="graph-info-item">
        <div class="graph-info-number graph-info--color2">
            57869 / 1483999
        </div>

        <span class="graph-info-text graph-info--color2">
            <div class="graph-info-square graph-info-square-color2"></div>
            Сельское хозяйство в тыс. $
        </span>
    </div>
</div>