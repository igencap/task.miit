<div class="card bg-white ">
    <div class="card-header">
        <span>Стикер (лично)</span>
    </div>
    <div id="sticky-note-container">
        <textarea name="note" cols="40" rows="10" id="sticky-note" class="sticky-note" style=""></textarea>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var $stickyNote = $("#sticky-note");

        var saveStickyNote = function () {
            $.ajax({
                url: "https://task.miit.uz/index.php/dashboard/save_sticky_note",
                data: { sticky_note: $stickyNote.val() },
                cache: false,
                type: 'POST'
            });
        };

        $stickyNote.change(function () {
            saveStickyNote();
        });

        //save sticky not on window refresh/tab close/browser close
        $stickyNote.keydown(function () {
            window.addEventListener("beforeunload", function (e) {
                saveStickyNote();
            });
        });
    });
</script>