<div class="card  bg-white">
    <div class="card-header" style="border-top-left-radius: 20px; border-top-right-radius: 20px;">
        <span>Мои проекты</span>
        <a href="#">Все</a>
    </div>
    <div class="card-body">
        <div class="projects-title-cover d-flex justify-content-between">

            <div class="porjects-title-img-cover d-flex">
                <div class="projects-icon">
                    <img class="projects-title-img" src="Главная _ Task Manager_files/projects-img.svg" alt="">
                </div>

                <span class="projects-inner ms-3">
                    <p class="projects-title">
                        Разработка чата
                    </p>
                    <a href="#" class="projects-tag">
                        Программирование
                    </a>
                </span>
            </div>
            <div class="projects-id">
                ID <span>7</span>
            </div>
        </div>
        <div class="prjects-list">
            <div class="projects-item">
                <div class="projects-members projects-item-name">
                    Участники
                </div>
                <div class="projects-members-img-cover">
                    <img class="projects-members-img" src="Главная _ Task Manager_files/example.jpg" alt="">
                    <img class="projects-members-img" src="Главная _ Task Manager_files/example2.jpg" alt="">
                    <img class="projects-members-img" src="Главная _ Task Manager_files/example.jpg" alt="">
                </div>
                <span>4</span>
            </div>

            <div class="projects-item">
                <div class="projects-deadline projects-item-name">
                    Сроки
                </div>
                <div class="projects-deadline-inner">
                    <span class="projects-deadline--arrow">20.02.2023</span>
                    <span>20.01.2023</span>
                </div>
            </div>

            <div class="projects-item">
                <div class="projects-status projects-item-name">
                    Статус
                </div>
                <span class="">
                    В процессе
                </span>
            </div>

            <div class="projects-progressbar-cover">
                <p class="projects-progressbar-title">
                    <span class="projects-progressbar-progress">Прогресс</span>
                    <span class="projects-progressbar-number">80%</span>
                </p>

                <div class="projects-progressbar">
                    <div class="projects-progressbar-inner"></div>
                </div>
            </div>

            <a href="#" class="btn projects-btn">Подробнее</a>
        </div>
    </div>
</div>