<div class="container-fluid">
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto mb-lg-0">
            <li class="nav-item">
                <a class="nav-link sidebar-toggle-btn" aria-current="page" href="#">
                    <img src="Главная _ Task Manager_files/burger.svg" alt="" class="feather feather-menu icon">
                </a>
            </li>
            <li class="nav-item">

            </li>
        </ul>

        <div class="d-flex w-auto">
            <ul class="navbar-nav">

                <script type="text/javascript" src="./Главная _ Task Manager_files/awesomplete.min.js"></script>

                <li class="nav-item dropdown menu__item menu__add-btn">
                    <a href="https://task.miit.uz/#" id="quick-add-icon"
                        class="nav-link dropdown-toggle menu__link" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <img src="Главная _ Task Manager_files/add-task.svg" alt=""
                            class="feather feather-plus-circle icon">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        <li>
                            <a href="https://task.miit.uz/#" class="dropdown-item clearfix" title="Добавить задачу"
                                id="js-quick-add-task" data-act="ajax-modal" data-title="Добавить задачу"
                                data-action-url="https://task.miit.uz/index.php/projects/task_modal_form">Добавить
                                задачу</a><a href="https://task.miit.uz/#" class="dropdown-item clearfix"
                                title="Добавить несколько задач" data-post-add_type="multiple"
                                id="js-quick-add-multiple-task" data-act="ajax-modal"
                                data-title="Добавить несколько задач"
                                data-action-url="https://task.miit.uz/index.php/projects/task_modal_form">Добавить
                                несколько задач</a><a href="https://task.miit.uz/#" class="dropdown-item clearfix"
                                title="Добавить событие" data-post-client_id="" id="js-quick-add-event"
                                data-act="ajax-modal" data-title="Добавить событие"
                                data-action-url="https://task.miit.uz/index.php/events/modal_form">Добавить
                                событие</a><a href="https://task.miit.uz/#" class="dropdown-item clearfix"
                                title="Добавить заметку" id="js-quick-add-note" data-act="ajax-modal"
                                data-title="Добавить заметку"
                                data-action-url="https://task.miit.uz/index.php/notes/modal_form">Добавить
                                заметку</a><a href="https://task.miit.uz/#" class="dropdown-item clearfix"
                                title="Добавить To do" id="js-quick-add-to-do" data-act="ajax-modal"
                                data-title="Добавить To do"
                                data-action-url="https://task.miit.uz/index.php/todo/modal_form">Добавить To
                                do</a><a href="https://task.miit.uz/#" class="dropdown-item clearfix"
                                title="Добавить заявку" id="js-quick-add-ticket" data-act="ajax-modal"
                                data-title="Добавить заявку"
                                data-action-url="https://task.miit.uz/index.php/tickets/modal_form">Добавить
                                заявку</a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item dropdown menu__item">
                    <a href="https://task.miit.uz/#" id="web-notification-icon"
                        class="nav-link dropdown-toggle menu__link" data-bs-toggle="dropdown">
                        <img src="Главная _ Task Manager_files/notification.svg" alt=""
                            class="feather feather-bell icon">
                    </a>
                    <div class="dropdown-menu dropdown-menu-end notification-dropdown w400">
                        <div class="dropdown-details card bg-white m0">
                            <div class="list-group">
                                <span class="list-group-item inline-loader p10"></span>
                            </div>
                        </div>
                        <div class="card-footer text-center mt-2">
                            <a href="https://task.miit.uz/index.php/notifications">Просмотреть все</a>
                        </div>
                    </div>
                </li>

                <li class="nav-item dropdown menu__item me-xl-0">
                    <a href="https://task.miit.uz/#" id="" class="nav-link dropdown-toggle menu__link menu__link-messages"
                        data-bs-toggle="dropdown">
                        <img src="Главная _ Task Manager_files/messages-blue.svg" alt=""
                            class="feather feather-mail icon">
                        <span class="badge bg-danger up">1</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-end w300">
                        <div class="dropdown-details card bg-white m0">
                            <div class="list-group">
                                <span class="list-group-item inline-loader p10"></span>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <a href="https://task.miit.uz/index.php/messages">Просмотреть все</a>
                        </div>
                    </div>
                </li>

                <li class="nav-item menu__item">
                    <a href="https://task.miit.uz/index.php/todo" class=" nav-link dropdown-toggle menu__link">
                        <img src="Главная _ Task Manager_files/check.svg" alt="">
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a id=" user-dropdown" href="https://task.miit.uz/#" class="nav-link dropdown-toggle user-link"
                        data-bs-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="avatar-xs avatar me-1">
                            <img alt="..." src="./Главная _ Task Manager_files/_file62c5272dcc735-avatar.png">
                        </span>
                        <span class="user-name ml10">Анвар Рахимов</span>
                        <img src="Главная _ Task Manager_files/arrow-down.svg" alt="">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end w200 profile-dropdown">
                        <li>
                            <a href="https://task.miit.uz/index.php/team_members/view/8/general" class="dropdown-item">
                                <img src="Главная _ Task Manager_files/Profile.svg" alt=""
                                    class="feather feather-user icon-16 me-2">
                                Мой профиль</a>
                        </li>
                        <li><a href="https://task.miit.uz/index.php/team_members/view/8/account" class="dropdown-item">
                                <img class="feather feather-key icon-16 me-2" src="Главная _ Task Manager_files/key.svg"
                                    alt="">
                                Изменить пароль</a></li>
                        <li><a href="https://task.miit.uz/index.php/team_members/view/8/my_preferences"
                                class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-settings icon-16 me-2">
                                    <circle cx="12" cy="12" r="3"></circle>
                                    <path
                                        d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                    </path>
                                </svg>Мои предпочтения</a></li>


                        <li class="dropdown-divider"></li>
                        <li class="pl10 ms-2 mt10 theme-changer">
                            <span class="color-tag clickable mr15 change-theme" data-color="F2F2F2"
                                style="background:#F2F2F2"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#00BCD4" data-color="00BCD4"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#17a589"
                                data-color="17a589"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#1E202D" data-color="1E202D"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#1d2632"
                                data-color="1d2632"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#2471a3" data-color="2471a3"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#2e4053"
                                data-color="2e4053"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#2e86c1" data-color="2e86c1"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#404040"
                                data-color="404040"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#555a61" data-color="555a61"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#557bbb"
                                data-color="557bbb"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#5d78ff" data-color="5d78ff"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#839192"
                                data-color="839192"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#83c340" data-color="83c340"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#884ea0"
                                data-color="884ea0"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#a6acaf" data-color="a6acaf"> </span><span
                                class="color-tag clickable mr15 change-theme" style="background:#a93226"
                                data-color="a93226"> </span><span class="color-tag clickable mr15 change-theme"
                                style="background:#d68910" data-color="d68910"> </span>
                        </li>


                        <li class="dropdown-divider"></li>
                        <li><a href="https://task.miit.uz/index.php/signin/sign_out" class="dropdown-item"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-log-out icon-16 me-2">
                                    <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                    <polyline points="16 17 21 12 16 7"></polyline>
                                    <line x1="21" y1="12" x2="9" y2="12"></line>
                                </svg> Выйти из системы</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--/.nav-collapse -->
</div>