<div class="card bg-white">

    <div class="card-header">
        <span>Моё настроение</span>
    </div>
    <div class="card-body d-flex justify-content-around">
        <a class="emoji-link-cover d-flex justify-content-around align-items-center flex-column" href="#">
            <div class="emoji-link-img-cover">
                <img class="emoji-link-img" src="Главная _ Task Manager_files/angry.svg" alt="">
            </div>
            <p class="emoji-text">
                Злой
            </p>
        </a>

        <a class="emoji-link-cover d-flex justify-content-around align-items-center flex-column" href="#">
            <div class="emoji-link-img-cover">
                <img class="emoji-link-img" src="Главная _ Task Manager_files/anxious.svg" alt="">
            </div>
            <p class="emoji-text">
                Тревожный
            </p>
        </a>

        <a class="emoji-link-cover d-flex justify-content-around align-items-center flex-column" href="#">
            <div class="emoji-link-img-cover">
                <img class="emoji-link-img" src="Главная _ Task Manager_files/anxious-2.svg" alt="">
            </div>
            <p class="emoji-text">
                Тревожный
            </p>
        </a>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        initScrollbar('#upcoming-event-container', {
            setHeight: 500
        });
    });
</script>