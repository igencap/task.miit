<div class="card bg-white">
    <div class="card-header">
        <span>Календарь</span>
        <a href="#">
            Посмотреть
        </a>
    </div>
    <div id="upcoming-event-container" class="ps">
        <div class="card-body">
            <div style="min-height: 190px;">
                <div class="calendar-item">
                    <div class="">
                        <p class="calendar-title">
                            Собрание
                        </p>
                        <p class="calendar-text">
                            Сегодня, 00:00 – 00:00
                        </p>
                    </div>
                </div>

                <div class="calendar-item">
                    <div class="">
                        <p class="calendar-title">
                            Собрание
                        </p>
                        <p class="calendar-text">
                            Сегодня, 00:00 – 00:00
                        </p>
                    </div>
                </div>

                <div class="calendar-item">
                    <div class="">
                        <p class="calendar-title">
                            Собрание
                        </p>
                        <p class="calendar-text">
                            Сегодня, 00:00 – 00:00
                        </p>
                    </div>
                </div>

                <div class="calendar-item">
                    <div class="">
                        <p class="calendar-title">
                            Собрание
                        </p>
                        <p class="calendar-text">
                            Сегодня, 00:00 – 00:00
                        </p>
                    </div>
                </div>

                <div class="calendar-item">
                    <div class="">
                        <p class="calendar-title">
                            Собрание
                        </p>
                        <p class="calendar-text">
                            Сегодня, 00:00 – 00:00
                        </p>
                    </div>
                </div>


                <!-- <div class="text-center">Не найдено ни одного события!</div>
                            <div class="text-center p15 text-off"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="10rem"
                                    height="10rem" viewBox="0 0 24 24" fill="none"
                                    stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-calendar">
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2">
                                    </rect>
                                    <line x1="16" y1="2" x2="16" y2="6"></line>
                                    <line x1="8" y1="2" x2="8" y2="6"></line>
                                    <line x1="3" y1="10" x2="21" y2="10"></line>
                                </svg></div> -->
            </div>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
            </div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        initScrollbar('#upcoming-event-container', {
            setHeight: 280
        });
    });
</script>