<div class="blog-card card-body-animated">
    <div class="card shadow news-animation">
        <a target="_blank"
            href="https://miit.uz/ru/news/zeppelin-international-ag-korxonasi-rahbariyati-bilan-uchrashuv-bolib-otdi">
            <img src="./Главная _ Task Manager_files/7a9278215895008cbfe286e66ae04a5d.jpg"
                class="card-img-top rounded-top news-item__img q-img__image q-img__image--with-transition q-img__image--loaded"
                alt="Состоялась встреча с руководством компании Zeppelin International AG"
                style="object-fit: cover; object-position: 50% 50%;">
        </a>
        <div class="card-body">
            <small class="d-block mb-2">2023-01-19</small>
            <h2 class="h5">
                <a target="_blank"
                    href="https://miit.uz/ru/news/zeppelin-international-ag-korxonasi-rahbariyati-bilan-uchrashuv-bolib-otdi">Состоялась
                    встреча с руководством компании Zeppelin
                    International
                    AG</a>
            </h2>
        </div>
    </div>
</div>