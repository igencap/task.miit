<div class="card bg-white">
    <div class="card-header">
        <span>Календарь</span>
        <a href="#">
            Посмотреть
        </a>
    </div>
    <div id="upcoming-event-container" class="ps">
        <div class="card-body">
            <div style="min-height: 190px;">
                <div class="text-center p15 text-off">
                    <svg width="77" height="85" viewBox="0 0 77 85" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.375 31.9445H74.4629" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M56.3654 47.7419H56.4029" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M38.4191 47.7419H38.4566" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M20.4338 47.7419H20.4712" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M56.3654 63.4606H56.4029" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M38.4191 63.4606H38.4566" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M20.4338 63.4606H20.4712" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M54.7552 2V15.3094" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path d="M22.0833 2V15.3094" stroke="#C8C8C8" stroke-width="4" stroke-linecap="round"
                            stroke-linejoin="round" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M55.5414 8.38672H21.2959C9.41861 8.38672 2 15.0032 2 27.1652V63.7659C2 76.1192 9.41861 82.8886 21.2959 82.8886H55.5039C67.4187 82.8886 74.7998 76.2339 74.7998 64.0719V27.1652C74.8373 15.0032 67.4561 8.38672 55.5414 8.38672Z"
                            stroke="#C8C8C8" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>

                    <div class="text-center">Нет событий</div>

                </div>
            </div>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
            </div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        initScrollbar('#upcoming-event-container', {
            setHeight: 280
        });
    });
</script>