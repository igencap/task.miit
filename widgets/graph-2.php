<script>
    am5.ready(function () {

        // Create root element
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("graph-2");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        // start and end angle must be set both for chart and series
        var chart = root.container.children.push(am5percent.PieChart.new(root, {
            startAngle: 180,
            endAngle: 360,
            layout: root.verticalLayout,
            innerRadius: am5.percent(50),
            radius: am5.percent(50)
        }));

        // Create series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
        // start and end angle must be set both for chart and series


        var series = chart.series.push(am5percent.PieSeries.new(root, {
            startAngle: 180,
            endAngle: 360,
            valueField: "value",
            categoryField: "category",
            alignLabels: false
        }));

        series.get("colors").set("colors", [
            am5.color(0xA83EE9),
            am5.color(0x2D64F3)
        ]);


        series.labels.template.set("visible", false);

        series.states.create("hidden", {
            startAngle: 180,
            endAngle: 180
        });

        series.slices.template.setAll({
            cornerRadius: 0
        });

        series.ticks.template.setAll({
            forceHidden: true
        });

        // Set data
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
        series.data.setAll([
            { value: 10, category: null },
            { value: 10, category: null },

        ]);

        series.appear(1000, 100);

    }); // end am5.ready()
</script>

<div class="graph-title">
    Импорт товаров/услуг
</div>

<div class="" id="graph-2"></div>

<div class="graph-info">
    <div class="graph-info-item">
        <span class="graph-info-number graph-info--color3">
            31 из 50 млн $
        </span>

        <span class="graph-info-text graph-info--color3">
            <div class="graph-info-square graph-info-square-color3"></div>
            Услуги
        </span>
    </div>

    <div class="graph-info-item">
        <div class="graph-info-number graph-info--color4">
            31 из 50 млн $
        </div>

        <span class="graph-info-text graph-info--color4">
            <div class="graph-info-square graph-info-square-color4"></div>
            Услуги
        </span>
    </div>
</div>