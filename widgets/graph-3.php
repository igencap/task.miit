<script>
    am5.ready(function () {

        // Create root element
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("graph-3");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        // start and end angle must be set both for chart and series
        var chart = root.container.children.push(am5percent.PieChart.new(root, {
            startAngle: 180,
            endAngle: 360,
            layout: root.verticalLayout,
            innerRadius: am5.percent(50),
            radius: am5.percent(50)
        }));


        // Create series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
        // start and end angle must be set both for chart and series

        var series = chart.series.push(am5percent.PieSeries.new(root, {
            startAngle: 180,
            endAngle: 360,
            valueField: "value",
            categoryField: "category",
            alignLabels: false
        }));

        series.get("colors").set("colors", [
            am5.color(0x53AD3D),
            am5.color(0xFBAE3B)
        ]);


        series.labels.template.set("visible", false);


        series.states.create("hidden", {
            startAngle: 180,
            endAngle: 180
        });

        series.slices.template.setAll({
            cornerRadius: 0
        });

        series.ticks.template.setAll({
            forceHidden: true
        });

        // Set data
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
        series.data.setAll([
            { value: 10, category: null, fill: am5.color(0x995522) },
            { value: 10, category: null },
        ]);


        series.appear(1000, 100);

    }); // end am5.ready()
</script>

<div class="graph-title">
    Экспорт по отраслям/регионам
</div>

<div class="" id="graph-3"></div>

<div class="graph-info">
    <div class="graph-info-item">
        <span class="graph-info-number graph-info--color5">
            31 из 50 млн $
        </span>

        <span class="graph-info-text graph-info--color5">
            <div class="graph-info-square graph-info-square-color5"></div>
            Услуги
        </span>
    </div>

    <div class="graph-info-item">
        <div class="graph-info-number graph-info--color6">
            31 из 50 млн $
        </div>

        <span class="graph-info-text graph-info--color6">
            <div class="graph-info-square graph-info-square-color6"></div>
            Услуги
        </span>
    </div>
</div>