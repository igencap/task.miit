<div class="blog-card card-body-animated">
    <div class="card shadow news-animation">
        <a target="_blank"
            href="https://miit.uz/ru/news/ozbekiston-va-aqsh-iqtisodiy-hamkorlikni-yolga-qoyishga-tayyor">
            <img src="./Главная _ Task Manager_files/62ea3266e62bdc132c9a2418fc100fa8.jpg"
                class="card-img-top rounded-top news-item__img q-img__image q-img__image--with-transition q-img__image--loaded"
                alt="Узбекистан и США готовы наращивать экономическое партнёрство"
                style="object-fit: cover; object-position: 50% 50%;">
        </a>
        <div class="card-body">
            <small class="d-block mb-2">2022-10-17</small>
            <h2 class="h5">
                <a target="_blank"
                    href="https://miit.uz/ru/news/ozbekiston-va-aqsh-iqtisodiy-hamkorlikni-yolga-qoyishga-tayyor">Узбекистан
                    и США готовы наращивать экономическое
                    партнёрство</a>
            </h2>
        </div>
    </div>
</div>