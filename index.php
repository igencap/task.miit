<!DOCTYPE html>
<!-- saved from url=(0021)https://task.miit.uz/ -->
<html lang="en" dir="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="fairsketch">
    <link rel="icon" href="./Главная _ Task Manager_files/_file62a887f329028-favicon.png">

    <title>
        Главная | Task Manager</title>

    <script type="text/javascript">
        AppHelper = {};
        AppHelper.baseUrl = "https://task.miit.uz";
        AppHelper.assetsDirectory = "https://task.miit.uz/assets/";
        AppHelper.settings = {};
        AppHelper.settings.firstDayOfWeek = "1" || 0;
        AppHelper.settings.currencySymbol = "$";
        AppHelper.settings.currencyPosition = "left" || "left";
        AppHelper.settings.decimalSeparator = ".";
        AppHelper.settings.thousandSeparator = "";
        AppHelper.settings.noOfDecimals = ("2" == "0") ? 0 : 2;
        AppHelper.settings.displayLength = "10";
        AppHelper.settings.dateFormat = "d.m.Y";
        AppHelper.settings.timeFormat = "24_hours";
        AppHelper.settings.scrollbar = "jquery";
        AppHelper.settings.enableRichTextEditor = "1";
        AppHelper.settings.notificationSoundVolume = "9";
        AppHelper.settings.disableKeyboardShortcuts = "0";
        AppHelper.userId = "8";
        AppHelper.notificationSoundSrc = "https://task.miit.uz/files/system/notification.mp3";

        //push notification
        AppHelper.settings.enablePushNotification = "";
        AppHelper.settings.userEnableWebNotification = "1";
        AppHelper.settings.userDisablePushNotification = "0";
        AppHelper.settings.pusherKey = "";
        AppHelper.settings.pusherCluster = "";
        AppHelper.settings.pushNotficationMarkAsReadUrl = "https://task.miit.uz/index.php/notifications/set_notification_status_as_read";
        AppHelper.https = "1";

        AppHelper.settings.disableResponsiveDataTableForMobile = "";
        AppHelper.settings.disableResponsiveDataTable = "";

        AppHelper.csrfTokenName = "rise_csrf_token";
        AppHelper.csrfHash = "4c51843ed8bdedf29a60b7aecec0e323";

        AppHelper.settings.defaultThemeColor = "2471a3";

        AppHelper.settings.timepickerMinutesInterval = 5;

        AppHelper.settings.weekends = "6,0";

        AppHelper.serviceWorkerUrl = "https://task.miit.uz/assets/js/sw/sw.js";

        AppHelper.uploadPastedImageLink = "https://task.miit.uz/index.php/upload_pasted_image/save";

    </script>
    <script type="text/javascript">
        AppLanugage = {};
        AppLanugage.locale = "ru";
        AppLanugage.localeLong = "ru-RU";

        AppLanugage.days = ["\u0412\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435", "\u041f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a", "\u0412\u0442\u043e\u0440\u043d\u0438\u043a", "\u0421\u0440\u0435\u0434\u0430", "\u0427\u0435\u0442\u0432\u0435\u0440\u0433", "\u041f\u044f\u0442\u043d\u0438\u0446\u0430", "\u0421\u0443\u0431\u0431\u043e\u0442\u0430"];
        AppLanugage.daysShort = ["\u0412\u0441", "\u041f\u043d", "\u0412\u0442", "\u0421\u0440", "\u0427\u0442", "\u041f\u0442", "\u0421\u0431"];
        AppLanugage.daysMin = ["\u0412\u0441", "\u041f\u043d", "\u0412\u0442", "\u0421\u0440", "\u0427\u0442", "\u041f\u0442", "\u0421\u0431"];

        AppLanugage.months = ["\u042f\u043d\u0432\u0430\u0440\u044c", "\u0424\u0435\u0432\u0440\u0430\u043b\u044c", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440\u0435\u043b\u044c", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433\u0443\u0441\u0442", "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c", "\u041e\u043a\u0442\u044f\u0431\u0440\u044c", "\u041d\u043e\u044f\u0431\u0440\u044c", "\u0414\u0435\u043a\u0430\u0431\u0440\u044c"];
        AppLanugage.monthsShort = ["\u042f\u043d\u0432", "\u0424\u0435\u0432", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433", "\u0421\u0435\u043d\u0442", "\u041e\u043a\u0442", "\u041d\u043e\u044f", "\u0414\u0435\u043a"];

        AppLanugage.today = "Сегодня";
        AppLanugage.yesterday = "Вчера";
        AppLanugage.tomorrow = "Завтра";

        AppLanugage.search = "Поиск";
        AppLanugage.noRecordFound = "Ни одна запись не найдена.";
        AppLanugage.print = "Печать";
        AppLanugage.excel = "Excel";
        AppLanugage.printButtonTooltip = "По окончании нажмите Escape.";

        AppLanugage.fileUploadInstruction = "Выберите и перетащите документы сюда <br /> (или кликните для обзора...)";
        AppLanugage.fileNameTooLong = "Название файла слишком длинное.";

        AppLanugage.custom = "Настраиваемый";
        AppLanugage.clear = "Пустой";

        AppLanugage.total = "Итого";
        AppLanugage.totalOfAllPages = "Общая сумма по всем страницам";

        AppLanugage.all = "Все";

        AppLanugage.preview_next_key = "Далее (клавиша со стрелкой вправо)";
        AppLanugage.preview_previous_key = "Предыдущий (Стрелка влево ключ)";

        AppLanugage.filters = "Фильтры";

        AppLanugage.comment = "Комментарий";

        AppLanugage.undo = "Undo";

    </script>
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/select2.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/select2-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/selectize.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/app.all.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/custom-style.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/fonts.css">
    <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/style.css">
    <link rel="stylesheet" type="text/css" href="./css/index.css">
    <script type="text/javascript" src="./Главная _ Task Manager_files/app.all.js"></script>


    <!-- Amchartjs -->
    <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
    <script src="Главная _ Task Manager_files/index.js"></script>
    <script src="Главная _ Task Manager_files/xy.js"></script>
    <script src="Главная _ Task Manager_files/radar.js"></script>
    <script src="Главная _ Task Manager_files/Animated.js"></script>
    <script src="Главная _ Task Manager_files/custom.js"></script>
    <script type="text/javascript" src="./Главная _ Task Manager_files/selectize.standalone.min.js"></script>
    <script>
        var data = {};
        data[AppHelper.csrfTokenName] = AppHelper.csrfHash;
        $.ajaxSetup({
            data: data
        });
    </script>


    <style type="text/css">
        /* Chart.js */
        /*
 * DOM element rendering detection
 * https://davidwalsh.name/detect-node-insertion
 */
        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99;
            }

            to {
                opacity: 1;
            }
        }

        .chartjs-render-monitor {
            animation: chartjs-render-animation 0.001s;
        }

        /*
 * DOM element resizing detection
 * https://github.com/marcj/css-element-queries
 */
        .chartjs-size-monitor,
        .chartjs-size-monitor-expand,
        .chartjs-size-monitor-shrink {
            position: absolute;
            direction: ltr;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            overflow: hidden;
            pointer-events: none;
            visibility: hidden;
            z-index: -1;
        }

        .chartjs-size-monitor-expand>div {
            position: absolute;
            width: 1000000px;
            height: 1000000px;
            left: 0;
            top: 0;
        }

        .chartjs-size-monitor-shrink>div {
            position: absolute;
            width: 200%;
            height: 200%;
            left: 0;
            top: 0;
        }
    </style>
</head>

<body class="page_dashboard" data-new-gr-c-s-check-loaded="14.1094.0" data-gr-ext-installed=""
    style="overflow: hidden; padding-right: 0px;" data-new-gr-c-s-loaded="14.1094.0">

    <script type="text/javascript" src="./Главная _ Task Manager_files/pusher.min.js"></script>

    <nav class="navbar navbar-expand fixed-top navbar-light navbar-custom" role="navigation" id="default-navbar">
        <?php include('widgets/menu.php') ?>
    </nav>

    <script type="text/javascript">
        //close navbar collapse panel on clicking outside of the panel
        $(document).click(function (e) {
            if (!$(e.target).is('#navbar') && isMobile()) {
                $('#navbar').collapse('hide');
            }
        });

        var notificationOptions = {};

        $(document).ready(function () {
            //load message notifications
            var messageOptions = {},
                messageIcon = "#message-notification-icon",
                notificationIcon = "#web-notification-icon";

            //check message notifications
            messageOptions.notificationUrl = "https://task.miit.uz/index.php/messages/count_notifications";
            messageOptions.notificationStatusUpdateUrl = "https://task.miit.uz/index.php/messages/update_notification_checking_status";
            messageOptions.checkNotificationAfterEvery = "60";
            messageOptions.icon = "mail";
            messageOptions.notificationSelector = $(messageIcon);
            messageOptions.isMessageNotification = true;

            checkNotifications(messageOptions);

            window.updateLastMessageCheckingStatus = function () {
                checkNotifications(messageOptions, true);
            };

            $('body').on('show.bs.dropdown', messageIcon, function () {
                messageOptions.notificationUrl = "https://task.miit.uz/index.php/messages/get_notifications";
                checkNotifications(messageOptions, true);
            });




            //check web notifications
            notificationOptions.notificationUrl = "https://task.miit.uz/index.php/notifications/count_notifications";
            notificationOptions.notificationStatusUpdateUrl = "https://task.miit.uz/index.php/notifications/update_notification_checking_status";
            notificationOptions.checkNotificationAfterEvery = "60";
            notificationOptions.icon = "bell";
            notificationOptions.notificationSelector = $(notificationIcon);
            notificationOptions.notificationType = "web";
            notificationOptions.pushNotification = "";

            checkNotifications(notificationOptions); //start checking notification after starting the message checking 

            if (isMobile()) {
                //for mobile devices, load the notifications list with the page load
                notificationOptions.notificationUrlForMobile = "https://task.miit.uz/index.php/notifications/get_notifications";
                checkNotifications(notificationOptions);
            }

            $('body').on('show.bs.dropdown', notificationIcon, function () {
                notificationOptions.notificationUrl = "https://task.miit.uz/index.php/notifications/get_notifications";
                checkNotifications(notificationOptions, true);
            });

            $('[data-bs-toggle="tooltip"]').tooltip();
        });

    </script>


    <div id="js-init-chat-icon" class="init-chat-icon">
        <!-- data-type= open/close/unread -->
        <span id="js-chat-min-icon" data-type="open" class="chat-min-icon"><svg xmlns="http://www.w3.org/2000/svg"
                width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle icon-18">
                <path
                    d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                </path>
            </svg></span>
    </div>

    <div id="js-rise-chat-wrapper" class="rise-chat-wrapper hide"></div>

    <script type="text/javascript">
        $(document).ready(function () {

            chatIconContent = {
                "open": "<i data-feather='message-circle' class='icon-18'></i>",
                "close": "<span class='chat-close'>&times;</span>",
                "unread": ""
            };

            //we'll wait for 15 sec after clicking on the unread icon to see more notifications again.

            setChatIcon = function (type, count) {

                //don't show count if the data-prevent-notification-count is 1
                if ($("#js-chat-min-icon").attr("data-prevent-notification-count") === "1" && type === "unread") {
                    return false;
                }


                $("#js-chat-min-icon").attr("data-type", type).html(count ? count : chatIconContent[type]);

                if (type === "open") {
                    $("#js-rise-chat-wrapper").addClass("hide"); //hide chat box
                    $("#js-init-chat-icon").removeClass("has-message");
                } else if (type === "close") {
                    $("#js-rise-chat-wrapper").removeClass("hide"); //show chat box
                    $("#js-init-chat-icon").removeClass("has-message");
                } else if (type === "unread") {
                    $("#js-init-chat-icon").addClass("has-message");
                }

            };

            changeChatIconPosition = function (type) {
                if (type === "close") {
                    $("#js-init-chat-icon").addClass("move-chat-icon");
                } else if (type === "open") {
                    $("#js-init-chat-icon").removeClass("move-chat-icon");
                }
            };

            //is there any active chat? open the popup
            //otherwise show the chat icon only
            var activeChatId = getCookie("active_chat_id"),
                isChatBoxOpen = getCookie("chatbox_open"),
                $chatIcon = $("#js-init-chat-icon");


            $chatIcon.click(function () {
                $("#js-rise-chat-wrapper").html("");

                window.updateLastMessageCheckingStatus();

                var $chatIcon = $("#js-chat-min-icon");

                if ($chatIcon.attr("data-type") === "unread") {
                    $chatIcon.attr("data-prevent-notification-count", "1");

                    //after clicking on the unread icon, we'll wait 11 sec to show more notifications again.
                    setTimeout(function () {
                        $chatIcon.attr("data-prevent-notification-count", "0");
                    }, 11000);
                }

                var windowSize = window.matchMedia("(max-width: 767px)");

                if ($chatIcon.attr("data-type") !== "close") {
                    //have to reload
                    setTimeout(function () {
                        loadChatTabs();
                    }, 200);
                    setChatIcon("close"); //show close icon
                    setCookie("chatbox_open", "1");
                    if (windowSize.matches) {
                        changeChatIconPosition("close");
                    }
                } else {
                    //have to close the chat box
                    setChatIcon("open"); //show open icon
                    setCookie("chatbox_open", "");
                    setCookie("active_chat_id", "");
                    if (windowSize.matches) {
                        changeChatIconPosition("open");
                    }
                }

                if (window.activeChatChecker) {
                    window.clearInterval(window.activeChatChecker);
                }

                if (typeof window.placeCartBox === "function") {
                    window.placeCartBox();
                }

                feather.replace();

            });

            //open chat box
            if (isChatBoxOpen) {

                if (activeChatId) {
                    getActiveChat(activeChatId);
                } else {
                    loadChatTabs();
                }
            }

            var windowSize = window.matchMedia("(max-width: 767px)");
            if (windowSize.matches) {
                if (isChatBoxOpen) {
                    $("#js-init-chat-icon").addClass("move-chat-icon");
                }
            }




            $('body #js-rise-chat-wrapper').on('click', '.js-message-row', function () {
                getActiveChat($(this).attr("data-id"));
            });

            $('body #js-rise-chat-wrapper').on('click', '.js-message-row-of-team-members-tab', function () {
                getChatlistOfUser($(this).attr("data-id"), "team_members");
            });

            $('body #js-rise-chat-wrapper').on('click', '.js-message-row-of-clients-tab', function () {
                getChatlistOfUser($(this).attr("data-id"), "clients");
            });


        });

        function getChatlistOfUser(user_id, tab_type) {

            setChatIcon("close"); //show close icon

            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/get_chatlist_of_user",
                type: "POST",
                data: { user_id: user_id, tab_type: tab_type },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);
                    appLoader.hide();
                }
            });
        }

        function loadChatTabs(trigger_from_user_chat) {

            setChatIcon("close"); //show close icon

            setCookie("active_chat_id", "");
            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/chat_list",
                data: {
                    type: "inbox"
                },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);

                    if (!trigger_from_user_chat) {
                        $("#chat-inbox-tab-button a").trigger("click");
                    } else if (trigger_from_user_chat === "team_members") {
                        $("#chat-users-tab-button").find("a").trigger("click");
                    } else if (trigger_from_user_chat === "clients") {
                        $("#chat-clients-tab-button").find("a").trigger("click");
                    }
                    appLoader.hide();
                }
            });

        }


        function getActiveChat(message_id) {
            setChatIcon("close"); //show close icon

            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/get_active_chat",
                type: "POST",
                data: {
                    message_id: message_id
                },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);
                    appLoader.hide();
                    setCookie("active_chat_id", message_id);
                    $("#js-chat-message-textarea").focus();
                }
            });
        }

        window.prepareUnreadMessageChatBox = function (totalMessages) {
            setChatIcon("unread", totalMessages); //show close icon
        };


        window.triggerActiveChat = function (message_id) {
            getActiveChat(message_id);
        }

    </script>



    <div id="left-menu-toggle-mask">

        <div class="sidebar sidebar-off">
            <?php include('widgets/sidebar.php') ?>
        </div><!-- sidebar menu end -->

        <script type="text/javascript">
            feather.replace();
        </script>
        <div class="page-container overflow-auto ">

            <div class="scrollable-page main-scrollable-page ps ps--active-y"
                style="height: 857px; position: relative;">
                <div id="page-content" class="page-wrapper clearfix">

                    <div class="help-search-box-container ">
                        <?php include('widgets/search.php') ?>
                    </div>

                    <div class="row mb">
                        <div class="col-xxl-10 col-12">
                            <div class="row">

                                <div class="col-xxl-2 col-lg-12 col-12 profile_widget_container">
                                    <div class="bg-white">
                                        <div class="row">
                                            <?php include('widgets/profile.php') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-10 col-12 graphs">
                                    <div class="row bg-white">
                                        <div class="col-xxl-4 col-lg-4 col-12">
                                            <div class="">
                                                <?php include('widgets/graph-1.php') ?>
                                            </div>
                                        </div>
                                        <div class="col-xxl-4 col-lg-4 col-12">
                                            <div class="">
                                                <?php include('widgets/graph-2.php') ?>
                                            </div>
                                        </div>
                                        <div class=" col-xxl-4 col-lg-4 col-12">
                                            <div class="">
                                                <?php include('widgets/graph-3.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-2 col-12 col-lg-12 projects_widget_container">
                            <div class="bg-white">
                                <?php include('widgets/projects-widget.php') ?>
                            </div>
                        </div>

                        <div class="col-xxl-10 col-lg-9">
                            <!-- Добавил класс tasks -->
                            <div class="row tasks">

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-1.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-2.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-3.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-4.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-5.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-6.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-7.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-8.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-9.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-10.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-11.php') ?>
                                    </div>
                                </div>

                                <div class="col-xxl-2 col-lg-3 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-12.php') ?>
                                    </div>
                                </div>

                                <h3 class="main-title clearfix">
                                    Полезные ссылки
                                </h3>

                                <div class="widget-container col-xxl-2 col-lg-6 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-13.php') ?>
                                    </div>
                                </div>
                                <div class="widget-container col-xxl-2 col-lg-6 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-14.php') ?>
                                    </div>
                                </div>
                                <div class="widget-container col-xxl-2 col-lg-6 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-15.php') ?>
                                    </div>
                                </div>
                                <div class="widget-container col-xxl-2 col-lg-6 col-12">
                                    <div class="bg-white">
                                        <?php include('widgets/widget-16.php') ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Добавил класс news__wrapper -->
                            <div class="col-xxl-12 col-sm-12 widget-container news__wrapper">
                                <h3 class="main-title">Новости</h3>
                                <div class="row">
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-1.php') ?>
                                    </div>
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-2.php') ?>
                                    </div>
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-3.php') ?>
                                    </div>
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-4.php') ?>
                                    </div>
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-5.php') ?>
                                    </div>
                                    <div class="col-xxl-2 col-12">
                                        <?php include('widgets/news-6.php') ?>
                                    </div>
                                </div>

                                <a class="news__btn" href="#">
                                    Посмотреть все
                                </a>
                            </div>
                        </div>

                        <div class="col-xxl-2 col-12">

                            <div class="bg-white">
                                <div class="col-xxl-12 widget-container">
                                    <?php include('widgets/emoji-widget.php') ?>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            initScrollbar('#upcoming-event-container', {
                                                setHeight: 500
                                            });
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class="bg-white">
                                <div class="col-xxl-12 widget-container">
                                    <?php include('widgets/calendar-widget.php') ?>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            initScrollbar('#upcoming-event-container', {
                                                setHeight: 280
                                            });
                                        });
                                    </script>
                                </div>
                            </div>


                            <div class="bg-white">
                                <div class="col-xxl-12 widget-container">
                                    <?php include('widgets/calendar-widget-empty.php') ?>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            initScrollbar('#upcoming-event-container', {
                                                setHeight: 280
                                            });
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class="bg-white">
                                <div class="col-xxl-12 widget-container birthday-tab">
                                    <?php include('widgets/birthday-widget.php') ?>
                                </div>
                            </div>

                            <!-- sticker -->
                            <div class="bg-white">
                                <div class="col-xxl-12 widget-container">
                                    <?php include('widgets/sticker-widget.php') ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            initScrollbar('#project-timeline-container', {
                                setHeight: 965
                            });

                            //update dashboard link
                            $(".dashboard-menu, .dashboard-image").closest("a").attr("href", window.location.href);

                        });
                    </script>

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; height: 914px; right: 0px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 454px;"></div>
                    </div>
                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; height: 857px; right: 0px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 399px;"></div>
                    </div>
                </div>

            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade hide" id="ajaxModal" aria-labelledby="ajaxModal" data-bs-focus="false"
            style="display: block;" aria-modal="true" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="ajaxModalTitle" data-title="Task Manager">Добавить заявку</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div id="ajaxModalContent">
                        <form action="https://task.miit.uz/index.php/tickets/save" id="ticket-form" class="general-form"
                            role="form" method="post" accept-charset="utf-8" novalidate="novalidate">
                            <input type="hidden" name="rise_csrf_token" value="4c51843ed8bdedf29a60b7aecec0e323">
                            <div id="new-ticket-dropzone" class="post-dropzone">
                                <div class="modal-body clearfix">
                                    <div class="container-fluid">
                                        <input type="hidden" name="id" value="">

                                        <div class="form-group">
                                            <div class="row">
                                                <label for="title" class=" col-xxl-3">Заголовок</label>
                                                <div class=" col-xxl-9">
                                                    <input type="text" name="title" value="" id="title"
                                                        class="form-control" placeholder="Заголовок" autofocus="1"
                                                        data-rule-required="1"
                                                        data-msg-required="Обязательное для заполнения поле.">
                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <div class="row">
                                                <label for="department_id" class=" col-xxl-3">Управления</label>
                                                <div class="col-xxl-9" id="porject-dropdown-section">
                                                    <div class="select2-container select2" id="s2id_autogen67"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                            <span class="select2-chosen" id="select2-chosen-68">Главное
                                                                информационно-аналитическое управление </span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen68"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-68" id="s2id_autogen68">
                                                        <div
                                                            class="select2-drop select2-display-none select2-with-searchbox">
                                                            <div class="select2-search"> <label
                                                                    for="s2id_autogen68_search"
                                                                    class="select2-offscreen"></label> <input
                                                                    type="text" autocomplete="off" autocorrect="off"
                                                                    autocapitalize="off" spellcheck="false"
                                                                    class="select2-input" role="combobox"
                                                                    aria-expanded="true" aria-autocomplete="list"
                                                                    aria-owns="select2-results-68"
                                                                    id="s2id_autogen68_search" placeholder=""> </div>
                                                            <ul class="select2-results" role="listbox"
                                                                id="select2-results-68">
                                                            </ul>
                                                        </div>
                                                    </div><select name="department_id" class="select2" tabindex="-1"
                                                        title="" style="display: none;">
                                                        <option value="2">Главное информационно-аналитическое управление
                                                        </option>
                                                        <option value="4">Главное управление международного
                                                            сотрудничества
                                                        </option>
                                                        <option value="8">Главное управление формирования и мониторинга
                                                            инвестиционных программ </option>
                                                        <option value="6">Департамент по сотрудничеству с Республикой
                                                            Корея
                                                        </option>
                                                        <option value="1">Управление ИКТ </option>
                                                        <option value="5">Управление координации региональных
                                                            подразделений
                                                        </option>
                                                        <option value="3">Управление по привлечению грантовых средств
                                                        </option>
                                                        <option value="9">Управление по развитию экспортного потенциала
                                                        </option>
                                                        <option value="7">Юридическое управление </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <label for="ticket_type_id" class=" col-xxl-3">Тип заявки</label>
                                                <div class="col-xxl-9">
                                                    <div class="select2-container select2" id="s2id_autogen69"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                            <span class="select2-chosen" id="select2-chosen-70">Служба
                                                                поддержки
                                                                IT </span><abbr
                                                                class="select2-search-choice-close"></abbr>
                                                            <span class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen70"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-70" id="s2id_autogen70">
                                                        <div
                                                            class="select2-drop select2-display-none select2-with-searchbox">
                                                            <div class="select2-search"> <label
                                                                    for="s2id_autogen70_search"
                                                                    class="select2-offscreen"></label> <input
                                                                    type="text" autocomplete="off" autocorrect="off"
                                                                    autocapitalize="off" spellcheck="false"
                                                                    class="select2-input" role="combobox"
                                                                    aria-expanded="true" aria-autocomplete="list"
                                                                    aria-owns="select2-results-70"
                                                                    id="s2id_autogen70_search" placeholder=""> </div>
                                                            <ul class="select2-results" role="listbox"
                                                                id="select2-results-70">
                                                            </ul>
                                                        </div>
                                                    </div><select name="ticket_type_id" class="select2" tabindex="-1"
                                                        title="" style="display: none;">
                                                        <option value="1">Служба поддержки IT </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- description can't be changed during editing -->
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="description" class=" col-xxl-3">Описание</label>
                                                <div class=" col-xxl-9">
                                                    <textarea name="description" cols="40" rows="10" id="description"
                                                        class="form-control" placeholder="Описание"
                                                        data-rule-required="1"
                                                        data-msg-required="Обязательное для заполнения поле."
                                                        data-rich-text-editor="1"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Assign to only visible to team members -->
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="assigned_to" class=" col-xxl-3">Закрепить за</label>
                                                <div class="col-xxl-9">
                                                    <div class="select2-container select2" id="s2id_autogen71"><a
                                                            href="javascript:void(0)" class="select2-choice"
                                                            tabindex="-1">
                                                            <span class="select2-chosen"
                                                                id="select2-chosen-72">-</span><abbr
                                                                class="select2-search-choice-close"></abbr> <span
                                                                class="select2-arrow" role="presentation"><b
                                                                    role="presentation"></b></span></a><label
                                                            for="s2id_autogen72"
                                                            class="select2-offscreen"></label><input
                                                            class="select2-focusser select2-offscreen" type="text"
                                                            aria-haspopup="true" role="button"
                                                            aria-labelledby="select2-chosen-72" id="s2id_autogen72">
                                                        <div
                                                            class="select2-drop select2-display-none select2-with-searchbox">
                                                            <div class="select2-search"> <label
                                                                    for="s2id_autogen72_search"
                                                                    class="select2-offscreen"></label> <input
                                                                    type="text" autocomplete="off" autocorrect="off"
                                                                    autocapitalize="off" spellcheck="false"
                                                                    class="select2-input" role="combobox"
                                                                    aria-expanded="true" aria-autocomplete="list"
                                                                    aria-owns="select2-results-72"
                                                                    id="s2id_autogen72_search" placeholder=""> </div>
                                                            <ul class="select2-results" role="listbox"
                                                                id="select2-results-72">
                                                            </ul>
                                                        </div>
                                                    </div><select name="assigned_to" class="select2" tabindex="-1"
                                                        title="" style="display: none;">
                                                        <option value="" selected="selected">-</option>
                                                        <option value="14">Азизбек Ахмедов</option>
                                                        <option value="8">Анвар Рахимов</option>
                                                        <option value="11">Виктор Кильдюшев</option>
                                                        <option value="10">Виталий Кулешов</option>
                                                        <option value="12">Глеб Нестеренко</option>
                                                        <option value="9">Дильшод Абдужалилов</option>
                                                        <option value="4">Мурад Сулейманов</option>
                                                        <option value="5">Накия Рахимова</option>
                                                        <option value="13">Павел Ерёмченко</option>
                                                        <option value="15">Темур Турсунбаев</option>
                                                        <option value="6">Улугбек Турсунов</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="post-file-dropzone-scrollbar hide">
                                            <div class="post-file-previews clearfix b-t" id="rnkuccqoxqowtbq">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <!-- file can't be uploaded during editing -->
                                    <button
                                        class="btn btn-default upload-file-button float-start me-auto btn-sm round dz-clickable"
                                        type="button" style="color:#7988a2" id="dyokkrluonqmrfx"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-camera icon-16">
                                            <path
                                                d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z">
                                            </path>
                                            <circle cx="12" cy="13" r="4"></circle>
                                        </svg> Загрузить файл</button>

                                    <button type="button" class="btn btn-default" data-bs-dismiss="modal"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-x icon-16">
                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                        </svg> Закрыть</button>
                                    <button type="submit" class="btn btn-primary"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-check-circle icon-16">
                                            <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                            <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                        </svg> Сохранить</button>
                                </div>
                            </div>
                        </form>


                        <script type="text/javascript">
                            $(document).ready(function () {

                                var uploadUrl = "https://task.miit.uz/index.php/tickets/upload_file";
                                var validationUrl = "https://task.miit.uz/index.php/tickets/validate_ticket_file";
                                var editMode = "";

                                var dropzone = attachDropzoneWithForm("#new-ticket-dropzone", uploadUrl, validationUrl);

                                $("#ticket-form").appForm({
                                    onSuccess: function (result) {
                                        if (editMode) {

                                            appAlert.success(result.message, { duration: 10000 });

                                            //don't reload whole page when it's the list view
                                            if ($("#ticket-table").length) {
                                                $("#ticket-table").appTable({ newData: result.data, dataId: result.id });
                                            } else {
                                                location.reload();
                                            }
                                        } else {
                                            $("#ticket-table").appTable({ newData: result.data, dataId: result.id });
                                        }

                                    }
                                });
                                setTimeout(function () {
                                    $("#title").focus();
                                }, 200);
                                $("#ticket-form .select2").select2();

                                $("#ticket_labels").select2({ multiple: true, data: [] });


                                //load all client contacts of selected client
                                $("#client_id").select2().on("change", function () {
                                    var client_id = $(this).val();
                                    if ($(this).val()) {
                                        $('#requested_by_id').select2("destroy");
                                        $("#requested_by_id").hide();
                                        appLoader.show({ container: "#requested-by-dropdown-section", zIndex: 1 });
                                        $.ajax({
                                            url: "https://task.miit.uz/index.php/tickets/get_client_contact_suggestion" + "/" + client_id,
                                            dataType: "json",
                                            success: function (result) {
                                                $("#requested_by_id").show().val("");
                                                $('#requested_by_id').select2({ data: result });
                                                appLoader.hide();
                                            }
                                        });
                                    }
                                });

                                $('#requested_by_id').select2({ data: [{ "id": "", "text": "-" }] });

                                if ("") {
                                    $("#client_id").select2("readonly", true);
                                }

                            });


                        </script>
                    </div>
                    <div id="ajaxModalOriginalContent" class="hide">
                        <div class="original-modal-body">
                            <div class="circle-loader"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-bs-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-backdrop fade hide"></div><input type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">

        <!-- Modal -->
        <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModal"
            aria-hidden="true">
            <div class="modal-dialog" style="max-width: 400px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="confirmationModalTitle">Удалить?</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div id="confirmationModalContent" class="modal-body">
                        <div class="container-fluid">
                            Вы уверены? Вы не сможете отменить это действие!
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button id="confirmDeleteButton" type="button" class="btn btn-danger"
                            data-bs-dismiss="modal"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 icon-16">
                                <polyline points="3 6 5 6 21 6"></polyline>
                                <path
                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                </path>
                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                <line x1="14" y1="11" x2="14" y2="17"></line>
                            </svg> Удалить</button>
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal"><svg
                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-x icon-16">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg> Отмена</button>
                    </div>
                </div>
            </div>
        </div>
        <link rel="stylesheet" type="text/css" href="./Главная _ Task Manager_files/summernote.css">
        <script type="text/javascript" src="./Главная _ Task Manager_files/summernote.min.js"></script>
        <script type="text/javascript" src="./Главная _ Task Manager_files/summernote-ru-RU.js"></script>
        <div style="display: none;">
            <script type="text/javascript">
                feather.replace();
            </script>
        </div>
        <input type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
        <div class="select2-drop select2-display-none select2-with-searchbox select2-drop-active"
            style="left: 776.875px; width: 559.5px; top: 182.125px; bottom: auto; display: none;">
            <div class="select2-search"> <label for="s2id_autogen14_search" class="select2-offscreen"></label> <input
                    type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                    class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list"
                    aria-owns="select2-results-14" id="s2id_autogen14_search" placeholder=""> </div>
            <ul class="select2-results" role="listbox" id="select2-results-14"></ul>
        </div>
        <div id="select2-drop-mask" class="select2-drop-mask" style="display: none;"></div>
        <div class="select2-drop select2-display-none select2-with-searchbox select2-drop-active"
            style="left: 776.875px; width: 559.5px; top: 232.125px; bottom: auto; display: none;">
            <div class="select2-search"> <label for="s2id_autogen16_search" class="select2-offscreen"></label> <input
                    type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                    class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list"
                    aria-owns="select2-results-16" id="s2id_autogen16_search" placeholder=""> </div>
            <ul class="select2-results" role="listbox" id="select2-results-16"></ul>
        </div><input type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><input
            type="file" multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><span
            role="status" aria-live="polite" class="select2-hidden-accessible"></span><input type="file"
            multiple="multiple" class="dz-hidden-input"
            style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">

</body>

<grammarly-desktop-integration data-grammarly-shadow-root="true"></grammarly-desktop-integration>

</html>