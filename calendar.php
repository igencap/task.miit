<!DOCTYPE html>
<!-- saved from url=(0037)https://task.miit.uz/index.php/events -->
<html lang="en" dir="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="fairsketch">
    <link rel="icon" href="./Календарь _ Task Manager_files/_file62a887f329028-favicon.png">

    <title>
        Календарь | Task Manager</title>

    <script type="text/javascript">
        AppHelper = {};
        AppHelper.baseUrl = "https://task.miit.uz";
        AppHelper.assetsDirectory = "https://task.miit.uz/assets/";
        AppHelper.settings = {};
        AppHelper.settings.firstDayOfWeek = "1" || 0;
        AppHelper.settings.currencySymbol = "$";
        AppHelper.settings.currencyPosition = "left" || "left";
        AppHelper.settings.decimalSeparator = ".";
        AppHelper.settings.thousandSeparator = "";
        AppHelper.settings.noOfDecimals = ("2" == "0") ? 0 : 2;
        AppHelper.settings.displayLength = "10";
        AppHelper.settings.dateFormat = "d.m.Y";
        AppHelper.settings.timeFormat = "24_hours";
        AppHelper.settings.scrollbar = "jquery";
        AppHelper.settings.enableRichTextEditor = "1";
        AppHelper.settings.notificationSoundVolume = "9";
        AppHelper.settings.disableKeyboardShortcuts = "0";
        AppHelper.userId = "1";
        AppHelper.notificationSoundSrc = "https://task.miit.uz/files/system/notification.mp3";

        //push notification
        AppHelper.settings.enablePushNotification = "";
        AppHelper.settings.userEnableWebNotification = "1";
        AppHelper.settings.userDisablePushNotification = "0";
        AppHelper.settings.pusherKey = "";
        AppHelper.settings.pusherCluster = "";
        AppHelper.settings.pushNotficationMarkAsReadUrl = "https://task.miit.uz/index.php/notifications/set_notification_status_as_read";
        AppHelper.https = "1";

        AppHelper.settings.disableResponsiveDataTableForMobile = "";
        AppHelper.settings.disableResponsiveDataTable = "";

        AppHelper.csrfTokenName = "rise_csrf_token";
        AppHelper.csrfHash = "eb7ac71259ab084c68c35f49616d168b";

        AppHelper.settings.defaultThemeColor = "2471a3";

        AppHelper.settings.timepickerMinutesInterval = 5;

        AppHelper.settings.weekends = "6,0";

        AppHelper.serviceWorkerUrl = "https://task.miit.uz/assets/js/sw/sw.js";

        AppHelper.uploadPastedImageLink = "https://task.miit.uz/index.php/upload_pasted_image/save";

    </script>
    <script type="text/javascript">
        AppLanugage = {};
        AppLanugage.locale = "ru";
        AppLanugage.localeLong = "ru-RU";

        AppLanugage.days = ["\u0412\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435", "\u041f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a", "\u0412\u0442\u043e\u0440\u043d\u0438\u043a", "\u0421\u0440\u0435\u0434\u0430", "\u0427\u0435\u0442\u0432\u0435\u0440\u0433", "\u041f\u044f\u0442\u043d\u0438\u0446\u0430", "\u0421\u0443\u0431\u0431\u043e\u0442\u0430"];
        AppLanugage.daysShort = ["\u0412\u0441", "\u041f\u043d", "\u0412\u0442", "\u0421\u0440", "\u0427\u0442", "\u041f\u0442", "\u0421\u0431"];
        AppLanugage.daysMin = ["\u0412\u0441", "\u041f\u043d", "\u0412\u0442", "\u0421\u0440", "\u0427\u0442", "\u041f\u0442", "\u0421\u0431"];

        AppLanugage.months = ["\u042f\u043d\u0432\u0430\u0440\u044c", "\u0424\u0435\u0432\u0440\u0430\u043b\u044c", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440\u0435\u043b\u044c", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433\u0443\u0441\u0442", "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c", "\u041e\u043a\u0442\u044f\u0431\u0440\u044c", "\u041d\u043e\u044f\u0431\u0440\u044c", "\u0414\u0435\u043a\u0430\u0431\u0440\u044c"];
        AppLanugage.monthsShort = ["\u042f\u043d\u0432", "\u0424\u0435\u0432", "\u041c\u0430\u0440\u0442", "\u0410\u043f\u0440", "\u041c\u0430\u0439", "\u0418\u044e\u043d\u044c", "\u0418\u044e\u043b\u044c", "\u0410\u0432\u0433", "\u0421\u0435\u043d\u0442", "\u041e\u043a\u0442", "\u041d\u043e\u044f", "\u0414\u0435\u043a"];

        AppLanugage.today = "Сегодня";
        AppLanugage.yesterday = "Вчера";
        AppLanugage.tomorrow = "Завтра";

        AppLanugage.search = "Поиск";
        AppLanugage.noRecordFound = "Ни одна запись не найдена.";
        AppLanugage.print = "Печать";
        AppLanugage.excel = "Excel";
        AppLanugage.printButtonTooltip = "По окончании нажмите Escape.";

        AppLanugage.fileUploadInstruction = "Выберите и перетащите документы сюда <br /> (или кликните для обзора...)";
        AppLanugage.fileNameTooLong = "Название файла слишком длинное.";

        AppLanugage.custom = "Настраиваемый";
        AppLanugage.clear = "Пустой";

        AppLanugage.total = "Итого";
        AppLanugage.totalOfAllPages = "Общая сумма по всем страницам";

        AppLanugage.all = "Все";

        AppLanugage.preview_next_key = "Далее (клавиша со стрелкой вправо)";
        AppLanugage.preview_previous_key = "Предыдущий (Стрелка влево ключ)";

        AppLanugage.filters = "Фильтры";

        AppLanugage.comment = "Комментарий";

        AppLanugage.undo = "Undo";

    </script>
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/select2.css">
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/select2-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/selectize.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/app.all.css">
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/custom-style.css">
    <script type="text/javascript" src="./Календарь _ Task Manager_files/app.all.js.Без названия"></script>
    <link id="custom-theme-color" class="custom-theme-color" rel="stylesheet"
        href="./Календарь _ Task Manager_files/2471a3.css" type="text/css">
    <script type="text/javascript"
        src="./Календарь _ Task Manager_files/selectize.standalone.min.js.Без названия"></script>
    <script>

        var data = {};
        data[AppHelper.csrfTokenName] = AppHelper.csrfHash;
        $.ajaxSetup({
            data: data
        });
    </script>



</head>

<body class="modal-open" data-new-gr-c-s-check-loaded="14.1094.0" data-gr-ext-installed=""
    style="overflow: hidden; padding-right: 0px;">

    <script type="text/javascript" src="./Календарь _ Task Manager_files/pusher.min.js.Без названия"></script>

    <nav class="navbar navbar-expand fixed-top navbar-light navbar-custom shadow-sm" role="navigation"
        id="default-navbar">
        <div class="container-fluid">
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav me-auto mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link sidebar-toggle-btn" aria-current="page"
                            href="https://task.miit.uz/index.php/events#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-menu icon">
                                <line x1="3" y1="12" x2="21" y2="12"></line>
                                <line x1="3" y1="6" x2="21" y2="6"></line>
                                <line x1="3" y1="18" x2="21" y2="18"></line>
                            </svg>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="https://task.miit.uz/index.php/todo" class=" nav-link dropdown-toggle"><svg
                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-check-circle icon">
                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
                            </svg></a>
                    </li>

                </ul>

                <div class="d-flex w-auto">
                    <ul class="navbar-nav">

                        <script type="text/javascript"
                            src="./Календарь _ Task Manager_files/awesomplete.min.js.Без названия"></script>
                        <li class="nav-item hidden-sm" data-bs-toggle="tooltip" data-placement="left"
                            aria-label="Поиск (/)">
                            <a href="https://task.miit.uz/index.php/events#" class="nav-link"
                                data-modal-title="Поиск (/)" data-post-hide-header="1" id="global-search-btn"
                                data-act="ajax-modal" data-title="Поиск (/)"
                                data-action-url="https://task.miit.uz/index.php/search/search_modal_form"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-search icon">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="https://task.miit.uz/index.php/events#" id="quick-add-icon"
                                class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-plus-circle icon">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="16"></line>
                                    <line x1="8" y1="12" x2="16" y2="12"></line>
                                </svg></a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li>
                                    <a href="https://task.miit.uz/index.php/events#" class="dropdown-item clearfix"
                                        title="Добавить задачу" id="js-quick-add-task" data-act="ajax-modal"
                                        data-title="Добавить задачу"
                                        data-action-url="https://task.miit.uz/index.php/projects/task_modal_form">Добавить
                                        задачу</a><a href="https://task.miit.uz/index.php/events#"
                                        class="dropdown-item clearfix" title="Добавить несколько задач"
                                        data-post-add_type="multiple" id="js-quick-add-multiple-task"
                                        data-act="ajax-modal" data-title="Добавить несколько задач"
                                        data-action-url="https://task.miit.uz/index.php/projects/task_modal_form">Добавить
                                        несколько задач</a><a href="https://task.miit.uz/index.php/events#"
                                        class="dropdown-item clearfix" title="Добавить событие" data-post-client_id=""
                                        id="js-quick-add-event" data-act="ajax-modal" data-title="Добавить событие"
                                        data-action-url="https://task.miit.uz/index.php/events/modal_form">Добавить
                                        событие</a><a href="https://task.miit.uz/index.php/events#"
                                        class="dropdown-item clearfix" title="Добавить заметку" id="js-quick-add-note"
                                        data-act="ajax-modal" data-title="Добавить заметку"
                                        data-action-url="https://task.miit.uz/index.php/notes/modal_form">Добавить
                                        заметку</a><a href="https://task.miit.uz/index.php/events#"
                                        class="dropdown-item clearfix" title="Добавить To do" id="js-quick-add-to-do"
                                        data-act="ajax-modal" data-title="Добавить To do"
                                        data-action-url="https://task.miit.uz/index.php/todo/modal_form">Добавить To
                                        do</a><a href="https://task.miit.uz/index.php/events#"
                                        class="dropdown-item clearfix" title="Добавить заявку" id="js-quick-add-ticket"
                                        data-act="ajax-modal" data-title="Добавить заявку"
                                        data-action-url="https://task.miit.uz/index.php/tickets/modal_form">Добавить
                                        заявку</a>
                                </li>
                            </ul>
                        </li>


                        <li class="nav-item dropdown">
                            <a href="https://task.miit.uz/index.php/events#" id="web-notification-icon"
                                class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-bell icon">
                                    <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                    <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                                </svg></a>
                            <div class="dropdown-menu dropdown-menu-end notification-dropdown w400">
                                <div class="dropdown-details card bg-white m0">
                                    <div class="list-group">
                                        <span class="list-group-item inline-loader p10"></span>
                                    </div>
                                </div>
                                <div class="card-footer text-center mt-2">
                                    <a href="https://task.miit.uz/index.php/notifications">Просмотреть все</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown hidden-sm ">
                            <a href="https://task.miit.uz/index.php/events#" id="message-notification-icon"
                                class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-mail icon">
                                    <path
                                        d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z">
                                    </path>
                                    <polyline points="22,6 12,13 2,6"></polyline>
                                </svg></a>
                            <div class="dropdown-menu dropdown-menu-end w300">
                                <div class="dropdown-details card bg-white m0">
                                    <div class="list-group">
                                        <span class="list-group-item inline-loader p10"></span>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <a href="https://task.miit.uz/index.php/messages">Просмотреть все</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="user-dropdown" href="https://task.miit.uz/index.php/events#"
                                class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button"
                                aria-expanded="false">
                                <span class="avatar-xs avatar me-1">
                                    <img alt="..." src="./Календарь _ Task Manager_files/avatar.jpg">
                                </span>
                                <span class="user-name ml10">Admin Admin</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end w200">
                                <li><a href="https://task.miit.uz/index.php/team_members/view/1/general"
                                        class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-user icon-16 me-2">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>Мой профиль</a></li>
                                <li><a href="https://task.miit.uz/index.php/team_members/view/1/account"
                                        class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-key icon-16 me-2">
                                            <path
                                                d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4">
                                            </path>
                                        </svg>Изменить пароль</a></li>
                                <li><a href="https://task.miit.uz/index.php/team_members/view/1/my_preferences"
                                        class="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-settings icon-16 me-2">
                                            <circle cx="12" cy="12" r="3"></circle>
                                            <path
                                                d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                            </path>
                                        </svg>Мои предпочтения</a></li>


                                <li class="dropdown-divider"></li>
                                <li class="pl10 ms-2 mt10 theme-changer">
                                    <span class="color-tag clickable mr15 change-theme" data-color="F2F2F2"
                                        style="background:#F2F2F2"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#00BCD4"
                                        data-color="00BCD4"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#17a589" data-color="17a589"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#1E202D"
                                        data-color="1E202D"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#1d2632" data-color="1d2632"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#2471a3"
                                        data-color="2471a3"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#2e4053" data-color="2e4053"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#2e86c1"
                                        data-color="2e86c1"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#404040" data-color="404040"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#555a61"
                                        data-color="555a61"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#557bbb" data-color="557bbb"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#5d78ff"
                                        data-color="5d78ff"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#839192" data-color="839192"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#83c340"
                                        data-color="83c340"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#884ea0" data-color="884ea0"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#a6acaf"
                                        data-color="a6acaf"> </span><span class="color-tag clickable mr15 change-theme"
                                        style="background:#a93226" data-color="a93226"> </span><span
                                        class="color-tag clickable mr15 change-theme" style="background:#d68910"
                                        data-color="d68910"> </span>
                                </li>


                                <li class="dropdown-divider"></li>
                                <li><a href="https://task.miit.uz/index.php/signin/sign_out" class="dropdown-item"><svg
                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-log-out icon-16 me-2">
                                            <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                            <polyline points="16 17 21 12 16 7"></polyline>
                                            <line x1="21" y1="12" x2="9" y2="12"></line>
                                        </svg> Выйти из системы</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <script type="text/javascript">
        //close navbar collapse panel on clicking outside of the panel
        $(document).click(function (e) {
            if (!$(e.target).is('#navbar') && isMobile()) {
                $('#navbar').collapse('hide');
            }
        });

        var notificationOptions = {};

        $(document).ready(function () {
            //load message notifications
            var messageOptions = {},
                messageIcon = "#message-notification-icon",
                notificationIcon = "#web-notification-icon";

            //check message notifications
            messageOptions.notificationUrl = "https://task.miit.uz/index.php/messages/count_notifications";
            messageOptions.notificationStatusUpdateUrl = "https://task.miit.uz/index.php/messages/update_notification_checking_status";
            messageOptions.checkNotificationAfterEvery = "60";
            messageOptions.icon = "mail";
            messageOptions.notificationSelector = $(messageIcon);
            messageOptions.isMessageNotification = true;

            checkNotifications(messageOptions);

            window.updateLastMessageCheckingStatus = function () {
                checkNotifications(messageOptions, true);
            };

            $('body').on('show.bs.dropdown', messageIcon, function () {
                messageOptions.notificationUrl = "https://task.miit.uz/index.php/messages/get_notifications";
                checkNotifications(messageOptions, true);
            });




            //check web notifications
            notificationOptions.notificationUrl = "https://task.miit.uz/index.php/notifications/count_notifications";
            notificationOptions.notificationStatusUpdateUrl = "https://task.miit.uz/index.php/notifications/update_notification_checking_status";
            notificationOptions.checkNotificationAfterEvery = "60";
            notificationOptions.icon = "bell";
            notificationOptions.notificationSelector = $(notificationIcon);
            notificationOptions.notificationType = "web";
            notificationOptions.pushNotification = "";

            checkNotifications(notificationOptions); //start checking notification after starting the message checking 

            if (isMobile()) {
                //for mobile devices, load the notifications list with the page load
                notificationOptions.notificationUrlForMobile = "https://task.miit.uz/index.php/notifications/get_notifications";
                checkNotifications(notificationOptions);
            }

            $('body').on('show.bs.dropdown', notificationIcon, function () {
                notificationOptions.notificationUrl = "https://task.miit.uz/index.php/notifications/get_notifications";
                checkNotifications(notificationOptions, true);
            });

            $('[data-bs-toggle="tooltip"]').tooltip();
        });

    </script>
    <div id="js-init-chat-icon" class="init-chat-icon">
        <!-- data-type= open/close/unread -->
        <span id="js-chat-min-icon" data-type="open" class="chat-min-icon"><svg xmlns="http://www.w3.org/2000/svg"
                width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle icon-18">
                <path
                    d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                </path>
            </svg></span>
    </div>

    <div id="js-rise-chat-wrapper" class="rise-chat-wrapper hide"></div>

    <script type="text/javascript">
        $(document).ready(function () {

            chatIconContent = {
                "open": "<i data-feather='message-circle' class='icon-18'></i>",
                "close": "<span class='chat-close'>&times;</span>",
                "unread": ""
            };

            //we'll wait for 15 sec after clicking on the unread icon to see more notifications again.

            setChatIcon = function (type, count) {

                //don't show count if the data-prevent-notification-count is 1
                if ($("#js-chat-min-icon").attr("data-prevent-notification-count") === "1" && type === "unread") {
                    return false;
                }


                $("#js-chat-min-icon").attr("data-type", type).html(count ? count : chatIconContent[type]);

                if (type === "open") {
                    $("#js-rise-chat-wrapper").addClass("hide"); //hide chat box
                    $("#js-init-chat-icon").removeClass("has-message");
                } else if (type === "close") {
                    $("#js-rise-chat-wrapper").removeClass("hide"); //show chat box
                    $("#js-init-chat-icon").removeClass("has-message");
                } else if (type === "unread") {
                    $("#js-init-chat-icon").addClass("has-message");
                }

            };

            changeChatIconPosition = function (type) {
                if (type === "close") {
                    $("#js-init-chat-icon").addClass("move-chat-icon");
                } else if (type === "open") {
                    $("#js-init-chat-icon").removeClass("move-chat-icon");
                }
            };

            //is there any active chat? open the popup
            //otherwise show the chat icon only
            var activeChatId = getCookie("active_chat_id"),
                isChatBoxOpen = getCookie("chatbox_open"),
                $chatIcon = $("#js-init-chat-icon");


            $chatIcon.click(function () {
                $("#js-rise-chat-wrapper").html("");

                window.updateLastMessageCheckingStatus();

                var $chatIcon = $("#js-chat-min-icon");

                if ($chatIcon.attr("data-type") === "unread") {
                    $chatIcon.attr("data-prevent-notification-count", "1");

                    //after clicking on the unread icon, we'll wait 11 sec to show more notifications again.
                    setTimeout(function () {
                        $chatIcon.attr("data-prevent-notification-count", "0");
                    }, 11000);
                }

                var windowSize = window.matchMedia("(max-width: 767px)");

                if ($chatIcon.attr("data-type") !== "close") {
                    //have to reload
                    setTimeout(function () {
                        loadChatTabs();
                    }, 200);
                    setChatIcon("close"); //show close icon
                    setCookie("chatbox_open", "1");
                    if (windowSize.matches) {
                        changeChatIconPosition("close");
                    }
                } else {
                    //have to close the chat box
                    setChatIcon("open"); //show open icon
                    setCookie("chatbox_open", "");
                    setCookie("active_chat_id", "");
                    if (windowSize.matches) {
                        changeChatIconPosition("open");
                    }
                }

                if (window.activeChatChecker) {
                    window.clearInterval(window.activeChatChecker);
                }

                if (typeof window.placeCartBox === "function") {
                    window.placeCartBox();
                }

                feather.replace();

            });

            //open chat box
            if (isChatBoxOpen) {

                if (activeChatId) {
                    getActiveChat(activeChatId);
                } else {
                    loadChatTabs();
                }
            }

            var windowSize = window.matchMedia("(max-width: 767px)");
            if (windowSize.matches) {
                if (isChatBoxOpen) {
                    $("#js-init-chat-icon").addClass("move-chat-icon");
                }
            }




            $('body #js-rise-chat-wrapper').on('click', '.js-message-row', function () {
                getActiveChat($(this).attr("data-id"));
            });

            $('body #js-rise-chat-wrapper').on('click', '.js-message-row-of-team-members-tab', function () {
                getChatlistOfUser($(this).attr("data-id"), "team_members");
            });

            $('body #js-rise-chat-wrapper').on('click', '.js-message-row-of-clients-tab', function () {
                getChatlistOfUser($(this).attr("data-id"), "clients");
            });


        });

        function getChatlistOfUser(user_id, tab_type) {

            setChatIcon("close"); //show close icon

            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/get_chatlist_of_user",
                type: "POST",
                data: { user_id: user_id, tab_type: tab_type },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);
                    appLoader.hide();
                }
            });
        }

        function loadChatTabs(trigger_from_user_chat) {

            setChatIcon("close"); //show close icon

            setCookie("active_chat_id", "");
            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/chat_list",
                data: {
                    type: "inbox"
                },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);

                    if (!trigger_from_user_chat) {
                        $("#chat-inbox-tab-button a").trigger("click");
                    } else if (trigger_from_user_chat === "team_members") {
                        $("#chat-users-tab-button").find("a").trigger("click");
                    } else if (trigger_from_user_chat === "clients") {
                        $("#chat-clients-tab-button").find("a").trigger("click");
                    }
                    appLoader.hide();
                }
            });

        }


        function getActiveChat(message_id) {
            setChatIcon("close"); //show close icon

            appLoader.show({ container: "#js-rise-chat-wrapper", css: "bottom: 40%; right: 35%;" });
            $.ajax({
                url: "https://task.miit.uz/index.php/messages/get_active_chat",
                type: "POST",
                data: {
                    message_id: message_id
                },
                success: function (response) {
                    $("#js-rise-chat-wrapper").html(response);
                    appLoader.hide();
                    setCookie("active_chat_id", message_id);
                    $("#js-chat-message-textarea").focus();
                }
            });
        }

        window.prepareUnreadMessageChatBox = function (totalMessages) {
            setChatIcon("unread", totalMessages); //show close icon
        };


        window.triggerActiveChat = function (message_id) {
            getActiveChat(message_id);
        }

    </script>



    <div id="left-menu-toggle-mask">
        <div class="sidebar sidebar-off">
            <a class="sidebar-toggle-btn hide" href="https://task.miit.uz/index.php/events#">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-menu icon mt-1 text-off">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
            </a>

            <a class="sidebar-brand brand-logo" href="https://task.miit.uz/index.php/dashboard"><img
                    class="dashboard-image" src="./Календарь _ Task Manager_files/_file62a887f328a97-site-logo.png"></a>
            <a class="sidebar-brand brand-logo-mini" href="https://task.miit.uz/index.php/dashboard"><img
                    class="dashboard-image" src="./Календарь _ Task Manager_files/_file62a887f329028-favicon.png"></a>

            <div class="sidebar-scroll ps" style="height: 857px; position: relative;">
                <ul id="sidebar-menu" class="sidebar-menu">

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/dashboard">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-monitor icon">
                                <rect x="2" y="3" width="20" height="14" rx="2" ry="2"></rect>
                                <line x1="8" y1="21" x2="16" y2="21"></line>
                                <line x1="12" y1="17" x2="12" y2="21"></line>
                            </svg>
                            <span class="menu-text ">Главная</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/presence">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-user-check icon">
                                <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="8.5" cy="7" r="4"></circle>
                                <polyline points="17 11 19 13 23 9"></polyline>
                            </svg>
                            <span class="menu-text ">Локации руководителей</span>
                        </a>
                    </li>

                    <li class="active    main">
                        <a href="https://task.miit.uz/index.php/events">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-calendar icon">
                                <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                <line x1="16" y1="2" x2="16" y2="6"></line>
                                <line x1="8" y1="2" x2="8" y2="6"></line>
                                <line x1="3" y1="10" x2="21" y2="10"></line>
                            </svg>
                            <span class="menu-text ">Календарь</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/government_program">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-briefcase icon">
                                <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
                                <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
                            </svg>
                            <span class="menu-text ">Гос. Программа</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/projects/all_projects">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-grid icon">
                                <rect x="3" y="3" width="7" height="7"></rect>
                                <rect x="14" y="3" width="7" height="7"></rect>
                                <rect x="14" y="14" width="7" height="7"></rect>
                                <rect x="3" y="14" width="7" height="7"></rect>
                            </svg>
                            <span class="menu-text ">Проекты</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/projects/all_tasks">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-check-circle icon">
                                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                <polyline points="22 4 12 14.01 9 11.01"></polyline>
                            </svg>
                            <span class="menu-text ">Задачи</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/notes">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-book icon">
                                <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
                                <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
                            </svg>
                            <span class="menu-text ">Заметки</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/gifts">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-award icon">
                                <circle cx="12" cy="8" r="7"></circle>
                                <polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88"></polyline>
                            </svg>
                            <span class="menu-text ">Подарки</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/messages">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-message-circle icon">
                                <path
                                    d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                                </path>
                            </svg>
                            <span class="menu-text ">Сообщения</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/team_members">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-users icon">
                                <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                <circle cx="9" cy="7" r="4"></circle>
                                <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                            </svg>
                            <span class="menu-text ">Команда</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/tickets">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-life-buoy icon">
                                <circle cx="12" cy="12" r="10"></circle>
                                <circle cx="12" cy="12" r="4"></circle>
                                <line x1="4.93" y1="4.93" x2="9.17" y2="9.17"></line>
                                <line x1="14.83" y1="14.83" x2="19.07" y2="19.07"></line>
                                <line x1="14.83" y1="9.17" x2="19.07" y2="4.93"></line>
                                <line x1="14.83" y1="9.17" x2="18.36" y2="5.64"></line>
                                <line x1="4.93" y1="19.07" x2="9.17" y2="14.83"></line>
                            </svg>
                            <span class="menu-text ">Служба поддержки</span>
                        </a>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/docs/dashboard">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-file icon">
                                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                <polyline points="13 2 13 9 20 9"></polyline>
                            </svg>
                            <span class="menu-text ">Документы</span>
                        </a>
                    </li>

                    <li class="  expand    main">
                        <a href="https://task.miit.uz/index.php//">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-pie-chart icon">
                                <path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path>
                                <path d="M22 12A10 10 0 0 0 12 2v10z"></path>
                            </svg>
                            <span class="menu-text ">Система</span>
                        </a>
                        <ul>
                            <li>
                                <a href="https://task.miit.uz/index.php/team">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-minus">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span>Команда</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://task.miit.uz/index.php/department">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-minus">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span>Управления</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://task.miit.uz/index.php/direction">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-minus">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span>Направление</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://task.miit.uz/index.php/ldap">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-minus">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                    <span>Active Directory</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="    main">
                        <a href="https://task.miit.uz/index.php/settings/general">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-settings icon">
                                <circle cx="12" cy="12" r="3"></circle>
                                <path
                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                </path>
                            </svg>
                            <span class="menu-text ">Настройки</span>
                        </a>
                    </li>
                </ul>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                </div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                </div>
            </div>
        </div><!-- sidebar menu end -->

        <script type="text/javascript">
            feather.replace();
        </script>
        <div class="page-container overflow-auto ">

            <div class="scrollable-page main-scrollable-page ps ps--active-y"
                style="height: 857px; position: relative;">
                <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/fullcalendar.min.css">
                <script type="text/javascript"
                    src="./Календарь _ Task Manager_files/fullcalendar.min.js.Без названия"></script>
                <script type="text/javascript"
                    src="./Календарь _ Task Manager_files/locales-all.min.js.Без названия"></script>
                <div id="page-content" class="page-wrapper clearfix">
                    <div class="card mb0">
                        <div class="page-title clearfix">
                            <h1>Календарь событий</h1>
                            <div class="title-button-group custom-toolbar">


                                <div id="calendar-filter-dropdown" class="float-start ">
                                    <div class="mr15 DTTT_container custom-toolbar"><span
                                            class="dropdown inline-block filter-multi-select"><button
                                                class="btn btn-default dropdown-toggle caret " type="button"
                                                data-bs-toggle="dropdown" aria-expanded="true">Тип события </button>
                                            <div class="dropdown-menu">
                                                <ul class="list-group" data-act="multiselect">
                                                    <li class="list-group-item clickable  active" data-name="undefined"
                                                        data-value="events">Календарь</li>
                                                    <li class="list-group-item clickable " data-name="undefined"
                                                        data-value="task_start_date">Дата начала задач</li>
                                                    <li class="list-group-item clickable " data-name="undefined"
                                                        data-value="task_deadline">Крайний срок выполнения задачи</li>
                                                    <li class="list-group-item clickable " data-name="undefined"
                                                        data-value="project_start_date">Дата начала проекта</li>
                                                    <li class="list-group-item clickable " data-name="undefined"
                                                        data-value="project_deadline">Срок сдачи проекта</li>
                                                </ul>
                                            </div>
                                        </span></div>
                                </div>



                                <a href="https://task.miit.uz/index.php/events#" class="btn btn-default add-btn"
                                    title="Добавить событие" data-backdrop="static" data-act="ajax-modal"
                                    data-title="Добавить событие"
                                    data-action-url="https://task.miit.uz/index.php/events/modal_form"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-plus-circle icon-16">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg> Добавить событие</a> <a href="https://task.miit.uz/index.php/events#"
                                    class="btn btn-default add-btn" title="Добавить документ" data-post-client_id=""
                                    data-act="ajax-modal" data-title="Добавить документ"
                                    data-action-url="https://task.miit.uz/index.php/events/docs_modal"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-plus-circle icon-16">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg> Добавить документ</a><a href="https://task.miit.uz/index.php/events#"
                                    class="btn btn-default add-btn" title="Добавить график работы"
                                    data-post-client_id="" data-act="ajax-modal" data-title="Добавить график работы"
                                    data-action-url="https://task.miit.uz/index.php/events/modal_form_min"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-plus-circle icon-16">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg> Добавить график работы</a>
                                <a href="https://task.miit.uz/index.php/events#" class="hide" id="add_event_hidden"
                                    title="Добавить событие" data-post-client_id="" data-act="ajax-modal"
                                    data-title="Добавить событие"
                                    data-action-url="https://task.miit.uz/index.php/events/modal_form"></a> <a
                                    href="https://task.miit.uz/index.php/events#" class="hide" id="show_event_hidden"
                                    data-post-client_id="" data-post-cycle="0" data-post-editable="1"
                                    title="Подробности события" data-act="ajax-modal" data-title="Подробности события"
                                    data-action-url="https://task.miit.uz/index.php/events/view"></a> <a
                                    href="https://task.miit.uz/index.php/events#" class="hide" data-post-id=""
                                    id="show_leave_hidden" data-act="ajax-modal" data-title=""
                                    data-action-url="https://task.miit.uz/index.php/leaves/application_details"></a> <a
                                    href="https://task.miit.uz/index.php/events#" class="hide" data-post-id=""
                                    id="show_task_hidden" data-modal-lg="1" data-act="ajax-modal" data-title=""
                                    data-action-url="https://task.miit.uz/index.php/projects/task_view"></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="event-calendar" class="fc fc-media-screen fc-direction-ltr fc-theme-standard"
                                style="height: 769px;">
                                <div class="fc-header-toolbar fc-toolbar fc-toolbar-ltr">
                                    <div class="fc-toolbar-chunk">
                                        <div class="fc-button-group"><button
                                                class="fc-prev-button fc-button fc-button-primary" type="button"
                                                aria-label="prev"><span
                                                    class="fc-icon fc-icon-chevron-left"></span></button><button
                                                class="fc-next-button fc-button fc-button-primary" type="button"
                                                aria-label="next"><span
                                                    class="fc-icon fc-icon-chevron-right"></span></button></div><button
                                            disabled="" class="fc-today-button fc-button fc-button-primary"
                                            type="button">Сегодня</button>
                                    </div>
                                    <div class="fc-toolbar-chunk">
                                        <h2 class="fc-toolbar-title">январь 2023 г.</h2>
                                    </div>
                                    <div class="fc-toolbar-chunk">
                                        <div class="fc-button-group"><button
                                                class="fc-dayGridMonth-button fc-button fc-button-primary fc-button-active"
                                                type="button">Месяц</button><button
                                                class="fc-timeGridWeek-button fc-button fc-button-primary"
                                                type="button">Неделя</button><button
                                                class="fc-timeGridDay-button fc-button fc-button-primary"
                                                type="button">День</button><button
                                                class="fc-list-button fc-button fc-button-primary"
                                                type="button">Повестка дня</button><button
                                                class="fc-minister-button fc-button fc-button-primary"
                                                type="button"></button></div>
                                    </div>
                                </div>
                                <div class="fc-view-harness fc-view-harness-active">
                                    <div class="fc-daygrid fc-dayGridMonth-view fc-view">
                                        <table class="fc-scrollgrid  fc-scrollgrid-liquid">
                                            <thead>
                                                <tr class="fc-scrollgrid-section fc-scrollgrid-section-header ">
                                                    <td>
                                                        <div class="fc-scroller-harness">
                                                            <div class="fc-scroller" style="overflow: hidden;">
                                                                <table class="fc-col-header " style="width: 1588px;">
                                                                    <colgroup></colgroup>
                                                                    <tbody>
                                                                        <tr>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-mon">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">пн</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-tue">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">вт</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-wed">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">ср</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-thu">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">чт</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-fri">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">пт</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-sat">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">сб</a>
                                                                                </div>
                                                                            </th>
                                                                            <th
                                                                                class="fc-col-header-cell fc-day fc-day-sun">
                                                                                <div class="fc-scrollgrid-sync-inner"><a
                                                                                        class="fc-col-header-cell-cushion ">вс</a>
                                                                                </div>
                                                                            </th>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr
                                                    class="fc-scrollgrid-section fc-scrollgrid-section-body  fc-scrollgrid-section-liquid">
                                                    <td>
                                                        <div class="fc-scroller-harness fc-scroller-harness-liquid">
                                                            <div class="fc-scroller fc-scroller-liquid-absolute"
                                                                style="overflow: hidden auto;">
                                                                <div class="fc-daygrid-body fc-daygrid-body-unbalanced "
                                                                    style="width: 1588px;">
                                                                    <table class="fc-scrollgrid-sync-table"
                                                                        style="width: 1588px; height: 694px;">
                                                                        <colgroup></colgroup>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past fc-day-other"
                                                                                    data-date="2022-12-26">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">26</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past fc-day-other"
                                                                                    data-date="2022-12-27">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">27</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past fc-day-other"
                                                                                    data-date="2022-12-28">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">28</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past fc-day-other"
                                                                                    data-date="2022-12-29">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">29</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past fc-day-other"
                                                                                    data-date="2022-12-30">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">30</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past fc-day-other"
                                                                                    data-date="2022-12-31">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">31</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                                    data-date="2023-01-01">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">1</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                                    data-date="2023-01-02">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">2</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                                    data-date="2023-01-03">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">3</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                                    data-date="2023-01-04">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">4</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                                    data-date="2023-01-05">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">5</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                                    data-date="2023-01-06">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">6</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                                    data-date="2023-01-07">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">7</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                                    data-date="2023-01-08">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">8</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                                    data-date="2023-01-09">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">9</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                                    data-date="2023-01-10">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">10</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                                    data-date="2023-01-11">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">11</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                                    data-date="2023-01-12">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">12</a>
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-events"
                                                                                            style="padding-bottom: 30.2031px;">
                                                                                            <div class="fc-daygrid-event-harness fc-daygrid-event-harness-abs"
                                                                                                style="right: -226.844px;">
                                                                                                <a class="fc-daygrid-event fc-daygrid-block-event fc-h-event fc-event fc-event-start fc-event-end fc-event-past"
                                                                                                    style="border-color: rgb(74, 138, 244); background-color: rgb(74, 138, 244);">
                                                                                                    <div
                                                                                                        class="fc-event-main">
                                                                                                        <span
                                                                                                            class="clickable p5 w100p inline-block"
                                                                                                            style="background-color: #4a8af4; color: #fff"><span><svg
                                                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                                                    width="24"
                                                                                                                    height="24"
                                                                                                                    viewBox="0 0 24 24"
                                                                                                                    fill="none"
                                                                                                                    stroke="currentColor"
                                                                                                                    stroke-width="2"
                                                                                                                    stroke-linecap="round"
                                                                                                                    stroke-linejoin="round"
                                                                                                                    class="feather feather-lock icon-16">
                                                                                                                    <rect
                                                                                                                        x="3"
                                                                                                                        y="11"
                                                                                                                        width="18"
                                                                                                                        height="11"
                                                                                                                        rx="2"
                                                                                                                        ry="2">
                                                                                                                    </rect>
                                                                                                                    <path
                                                                                                                        d="M7 11V7a5 5 0 0 1 10 0v4">
                                                                                                                    </path>
                                                                                                                </svg>
                                                                                                                Командировка
                                                                                                                Термез</span></span>
                                                                                                    </div>
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                                    data-date="2023-01-13">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">13</a>
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-events"
                                                                                            style="padding-bottom: 30.2031px;">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                                    data-date="2023-01-14">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">14</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                                    data-date="2023-01-15">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">15</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                                    data-date="2023-01-16">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">16</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                                    data-date="2023-01-17">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">17</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                                    data-date="2023-01-18">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">18</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                                    data-date="2023-01-19">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">19</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                                    data-date="2023-01-20">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">20</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                                    data-date="2023-01-21">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">21</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                                    data-date="2023-01-22">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">22</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-past"
                                                                                    data-date="2023-01-23">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">23</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-past"
                                                                                    data-date="2023-01-24">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">24</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                            <div
                                                                                                class="fc-daygrid-event-harness">
                                                                                                <a
                                                                                                    class="fc-daygrid-event fc-daygrid-dot-event fc-event fc-event-start fc-event-end fc-event-past"><span
                                                                                                        class="clickable p5 w100p inline-block"
                                                                                                        style="background-color: #f1c40f; color: #fff"><span><svg
                                                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                                                width="24"
                                                                                                                height="24"
                                                                                                                viewBox="0 0 24 24"
                                                                                                                fill="none"
                                                                                                                stroke="currentColor"
                                                                                                                stroke-width="2"
                                                                                                                stroke-linecap="round"
                                                                                                                stroke-linejoin="round"
                                                                                                                class="feather feather-lock icon-16">
                                                                                                                <rect
                                                                                                                    x="3"
                                                                                                                    y="11"
                                                                                                                    width="18"
                                                                                                                    height="11"
                                                                                                                    rx="2"
                                                                                                                    ry="2">
                                                                                                                </rect>
                                                                                                                <path
                                                                                                                    d="M7 11V7a5 5 0 0 1 10 0v4">
                                                                                                                </path>
                                                                                                            </svg> Зум с
                                                                                                            инвестором</span></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-past"
                                                                                    data-date="2023-01-25">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">25</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-past"
                                                                                    data-date="2023-01-26">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">26</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-past"
                                                                                    data-date="2023-01-27">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">27</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-past"
                                                                                    data-date="2023-01-28">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">28</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-past"
                                                                                    data-date="2023-01-29">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">29</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="fc-daygrid-day fc-day fc-day-mon fc-day-today "
                                                                                    data-date="2023-01-30">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">30</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-tue fc-day-future"
                                                                                    data-date="2023-01-31">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">31</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-wed fc-day-future fc-day-other"
                                                                                    data-date="2023-02-01">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">1</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-thu fc-day-future fc-day-other"
                                                                                    data-date="2023-02-02">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">2</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-fri fc-day-future fc-day-other"
                                                                                    data-date="2023-02-03">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">3</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sat fc-day-future fc-day-other"
                                                                                    data-date="2023-02-04">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">4</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td class="fc-daygrid-day fc-day fc-day-sun fc-day-future fc-day-other"
                                                                                    data-date="2023-02-05">
                                                                                    <div
                                                                                        class="fc-daygrid-day-frame fc-scrollgrid-sync-inner">
                                                                                        <div class="fc-daygrid-day-top">
                                                                                            <a
                                                                                                class="fc-daygrid-day-number">5</a>
                                                                                        </div>
                                                                                        <div
                                                                                            class="fc-daygrid-day-events">
                                                                                        </div>
                                                                                        <div class="fc-daygrid-day-bg">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    var filterValues = "",
                        eventLabel = "";

                    var loadCalendar = function () {
                        var filter_values = filterValues || "events",
                            $eventCalendar = document.getElementById('event-calendar'),
                            event_label = eventLabel || "0";

                        appLoader.show();

                        window.fullCalendar = new FullCalendar.Calendar($eventCalendar, {
                            locale: AppLanugage.locale,
                            height: $(window).height() - 210,
                            headerToolbar: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'dayGridMonth,timeGridWeek,timeGridDay,list,minister'     // listMonth
                            },
                            events: "https://task.miit.uz/index.php/events/calendar_events/" + filter_values + "/" + event_label + "/" + "/",
                            dayMaxEvents: false,
                            dateClick: function (date, jsEvent, view) {
                                $("#add_event_hidden").attr("data-post-start_date", moment(date.date).format("YYYY-MM-DD"));
                                var startTime = moment(date.date).format("HH:mm:ss");
                                if (startTime === "00:00:00") {
                                    startTime = "";
                                }
                                $("#add_event_hidden").attr("data-post-start_time", startTime);
                                var endDate = moment(date.date).add(1, 'hours');

                                $("#add_event_hidden").attr("data-post-end_date", endDate.format("YYYY-MM-DD"));
                                var endTime = "";
                                if (startTime != "") {
                                    endTime = endDate.format("HH:mm:ss");
                                }

                                $("#add_event_hidden").attr("data-post-end_time", endTime);
                                $("#add_event_hidden").trigger("click");
                            },
                            eventClick: function (calEvent) {
                                calEvent = calEvent.event.extendedProps;
                                if (calEvent.event_type === "event") {
                                    $("#show_event_hidden").attr("data-post-id", calEvent.encrypted_event_id);
                                    $("#show_event_hidden").attr("data-post-cycle", calEvent.cycle);
                                    $("#show_event_hidden").trigger("click");

                                } else if (calEvent.event_type === "leave") {
                                    $("#show_leave_hidden").attr("data-post-id", calEvent.leave_id);
                                    $("#show_leave_hidden").trigger("click");

                                } else if (calEvent.event_type === "project_deadline" || calEvent.event_type === "project_start_date") {
                                    window.location = "https://task.miit.uz/index.php/projects/view/" + calEvent.project_id;
                                } else if (calEvent.event_type === "task_deadline" || calEvent.event_type === "task_start_date") {

                                    $("#show_task_hidden").attr("data-post-id", calEvent.task_id);
                                    $("#show_task_hidden").trigger("click");
                                }
                            },
                            eventContent: function (element) {
                                var icon = element.event.extendedProps.icon;
                                var title = element.event.title;
                                var color = element.event.backgroundColor;
                                if (icon) {
                                    title = "<span class='clickable p5 w100p inline-block' style='background-color: " + color + "; color: #fff'><span><i data-feather='" + icon + "' class='icon-16'></i> " + title + "</span></span>";
                                } else if (color) {
                                    title = "<span class='clickable p5 w100p inline-block' style='background-color: " + color + "; color: #fff'><span>" + title + "</span></span>";
                                }
                                return {
                                    html: title
                                };
                            },
                            loading: function (state) {
                                if (state === false) {
                                    appLoader.hide();
                                    setTimeout(function () {
                                        feather.replace();
                                    }, 100);
                                }
                            },
                            firstDay: AppHelper.settings.firstDayOfWeek
                        });

                        window.fullCalendar.render();
                    };

                    $(document).ready(function () {
                        $("#calendar-filter-dropdown").appMultiSelect({
                            text: "Тип события",
                            options: [{ "id": "events", "text": "\u041a\u0430\u043b\u0435\u043d\u0434\u0430\u0440\u044c", "isChecked": true }, { "id": "task_start_date", "text": "\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430 \u0437\u0430\u0434\u0430\u0447", "isChecked": false }, { "id": "task_deadline", "text": "\u041a\u0440\u0430\u0439\u043d\u0438\u0439 \u0441\u0440\u043e\u043a \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u0437\u0430\u0434\u0430\u0447\u0438", "isChecked": false }, { "id": "project_start_date", "text": "\u0414\u0430\u0442\u0430 \u043d\u0430\u0447\u0430\u043b\u0430 \u043f\u0440\u043e\u0435\u043a\u0442\u0430", "isChecked": false }, { "id": "project_deadline", "text": "\u0421\u0440\u043e\u043a \u0441\u0434\u0430\u0447\u0438 \u043f\u0440\u043e\u0435\u043a\u0442\u0430", "isChecked": false }],
                            onChange: function (values) {
                                filterValues = values.join('-');
                                loadCalendar();
                                setCookie("calendar_filters_of_user_1", values.join('-')); //save filters on browser cookie
                            },
                            onInit: function (values) {
                                filterValues = values.join('-');
                                loadCalendar();
                            }
                        });

                        var client = "";
                        if (client) {
                            setTimeout(function () {
                                window.fullCalendar.today();
                            });
                        }

                        //autoload the event popover
                        var encrypted_event_id = "";
                        if (encrypted_event_id) {
                            $("#show_event_hidden").attr("data-post-id", encrypted_event_id);
                            $("#show_event_hidden").trigger("click");
                        }

                        $("#event-labels-dropdown").select2({
                            data: [{ "id": "", "text": "- Event label -" }]
                        }).on("change", function () {
                            eventLabel = $(this).val();
                            loadCalendar();
                        });

                        $("#event-calendar .fc-header-toolbar .fc-button").click(function () {
                            feather.replace();
                        });
                    });
                </script>
                <div class="ps__rail-x" style="left: 0px; bottom: -1px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 1px; right: 0px; height: 914px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 1px; height: 913px;"></div>
                </div>
                <div class="ps__rail-x" style="left: 0px; bottom: -1px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 1px; height: 857px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 802px;"></div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade show" id="ajaxModal" role="dialog" aria-labelledby="ajaxModal" data-bs-focus="false"
        aria-modal="true" style="display: block;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ajaxModalTitle" data-title="Task Manager">Добавить заявку</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div id="ajaxModalContent">
                    <form action="https://task.miit.uz/index.php/tickets/save" id="ticket-form" class="general-form"
                        role="form" method="post" accept-charset="utf-8" novalidate="novalidate">
                        <input type="hidden" name="rise_csrf_token" value="eb7ac71259ab084c68c35f49616d168b">
                        <div id="new-ticket-dropzone" class="post-dropzone">
                            <div class="modal-body clearfix">
                                <div class="container-fluid">
                                    <input type="hidden" name="id" value="">

                                    <div class="form-group">
                                        <div class="row">
                                            <label for="title" class=" col-md-3">Заголовок</label>
                                            <div class=" col-md-9">
                                                <input type="text" name="title" value="" id="title" class="form-control"
                                                    placeholder="Заголовок" autofocus="1" data-rule-required="1"
                                                    data-msg-required="Обязательное для заполнения поле.">
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="row">
                                            <label for="department_id" class=" col-md-3">Управления</label>
                                            <div class="col-md-9" id="porject-dropdown-section">
                                                <div class="select2-container select2" id="s2id_autogen1"><a
                                                        href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                        <span class="select2-chosen" id="select2-chosen-2">Главное
                                                            информационно-аналитическое управление </span><abbr
                                                            class="select2-search-choice-close"></abbr> <span
                                                            class="select2-arrow" role="presentation"><b
                                                                role="presentation"></b></span></a><label
                                                        for="s2id_autogen2" class="select2-offscreen"></label><input
                                                        class="select2-focusser select2-offscreen" type="text"
                                                        aria-haspopup="true" role="button"
                                                        aria-labelledby="select2-chosen-2" id="s2id_autogen2">
                                                    <div
                                                        class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"> <label for="s2id_autogen2_search"
                                                                class="select2-offscreen"></label> <input type="text"
                                                                autocomplete="off" autocorrect="off"
                                                                autocapitalize="off" spellcheck="false"
                                                                class="select2-input" role="combobox"
                                                                aria-expanded="true" aria-autocomplete="list"
                                                                aria-owns="select2-results-2" id="s2id_autogen2_search"
                                                                placeholder=""> </div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-2"> </ul>
                                                    </div>
                                                </div><select name="department_id" class="select2" tabindex="-1"
                                                    title="" style="display: none;">
                                                    <option value="2">Главное информационно-аналитическое управление
                                                    </option>
                                                    <option value="4">Главное управление международного сотрудничества
                                                    </option>
                                                    <option value="8">Главное управление формирования и мониторинга
                                                        инвестиционных программ </option>
                                                    <option value="6">Департамент по сотрудничеству с Республикой Корея
                                                    </option>
                                                    <option value="1">Управление ИКТ </option>
                                                    <option value="5">Управление координации региональных подразделений
                                                    </option>
                                                    <option value="3">Управление по привлечению грантовых средств
                                                    </option>
                                                    <option value="9">Управление по развитию экспортного потенциала
                                                    </option>
                                                    <option value="7">Юридическое управление </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <label for="ticket_type_id" class=" col-md-3">Тип заявки</label>
                                            <div class="col-md-9">
                                                <div class="select2-container select2" id="s2id_autogen3"><a
                                                        href="javascript:void(0)" class="select2-choice" tabindex="-1">
                                                        <span class="select2-chosen" id="select2-chosen-4">Служба
                                                            поддержки IT </span><abbr
                                                            class="select2-search-choice-close"></abbr> <span
                                                            class="select2-arrow" role="presentation"><b
                                                                role="presentation"></b></span></a><label
                                                        for="s2id_autogen4" class="select2-offscreen"></label><input
                                                        class="select2-focusser select2-offscreen" type="text"
                                                        aria-haspopup="true" role="button"
                                                        aria-labelledby="select2-chosen-4" id="s2id_autogen4">
                                                    <div
                                                        class="select2-drop select2-display-none select2-with-searchbox">
                                                        <div class="select2-search"> <label for="s2id_autogen4_search"
                                                                class="select2-offscreen"></label> <input type="text"
                                                                autocomplete="off" autocorrect="off"
                                                                autocapitalize="off" spellcheck="false"
                                                                class="select2-input" role="combobox"
                                                                aria-expanded="true" aria-autocomplete="list"
                                                                aria-owns="select2-results-4" id="s2id_autogen4_search"
                                                                placeholder=""> </div>
                                                        <ul class="select2-results" role="listbox"
                                                            id="select2-results-4"> </ul>
                                                    </div>
                                                </div><select name="ticket_type_id" class="select2" tabindex="-1"
                                                    title="" style="display: none;">
                                                    <option value="1">Служба поддержки IT </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- description can't be changed during editing -->
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="description" class=" col-md-3">Описание</label>
                                            <div class=" col-md-9">
                                                <textarea name="description" cols="40" rows="10" id="description"
                                                    class="form-control" placeholder="Описание" data-rule-required="1"
                                                    data-msg-required="Обязательное для заполнения поле."
                                                    data-rich-text-editor="1"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Assign to only visible to team members -->



                                    <div class="post-file-dropzone-scrollbar hide">
                                        <div class="post-file-previews clearfix b-t" id="wrliqaigbpupoqb">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">

                                <!-- file can't be uploaded during editing -->
                                <button
                                    class="btn btn-default upload-file-button float-start me-auto btn-sm round dz-clickable"
                                    type="button" style="color:#7988a2" id="monwqjdnrimbvee"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-camera icon-16">
                                        <path
                                            d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z">
                                        </path>
                                        <circle cx="12" cy="13" r="4"></circle>
                                    </svg> Загрузить файл</button>

                                <button type="button" class="btn btn-default" data-bs-dismiss="modal"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-x icon-16">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg> Закрыть</button>
                                <button type="submit" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg"
                                        width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                        class="feather feather-check-circle icon-16">
                                        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                        <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                    </svg> Сохранить</button>
                            </div>
                        </div>
                    </form>


                    <script type="text/javascript">
                        $(document).ready(function () {

                            var uploadUrl = "https://task.miit.uz/index.php/tickets/upload_file";
                            var validationUrl = "https://task.miit.uz/index.php/tickets/validate_ticket_file";
                            var editMode = "";

                            var dropzone = attachDropzoneWithForm("#new-ticket-dropzone", uploadUrl, validationUrl);

                            $("#ticket-form").appForm({
                                onSuccess: function (result) {
                                    if (editMode) {

                                        appAlert.success(result.message, { duration: 10000 });

                                        //don't reload whole page when it's the list view
                                        if ($("#ticket-table").length) {
                                            $("#ticket-table").appTable({ newData: result.data, dataId: result.id });
                                        } else {
                                            location.reload();
                                        }
                                    } else {
                                        $("#ticket-table").appTable({ newData: result.data, dataId: result.id });
                                    }

                                }
                            });
                            setTimeout(function () {
                                $("#title").focus();
                            }, 200);
                            $("#ticket-form .select2").select2();

                            $("#ticket_labels").select2({ multiple: true, data: [] });


                            //load all client contacts of selected client
                            $("#client_id").select2().on("change", function () {
                                var client_id = $(this).val();
                                if ($(this).val()) {
                                    $('#requested_by_id').select2("destroy");
                                    $("#requested_by_id").hide();
                                    appLoader.show({ container: "#requested-by-dropdown-section", zIndex: 1 });
                                    $.ajax({
                                        url: "https://task.miit.uz/index.php/tickets/get_client_contact_suggestion" + "/" + client_id,
                                        dataType: "json",
                                        success: function (result) {
                                            $("#requested_by_id").show().val("");
                                            $('#requested_by_id').select2({ data: result });
                                            appLoader.hide();
                                        }
                                    });
                                }
                            });

                            $('#requested_by_id').select2({ data: [{ "id": "", "text": "-" }] });

                            if ("") {
                                $("#client_id").select2("readonly", true);
                            }

                        });


                    </script>
                </div>
                <div id="ajaxModalOriginalContent" class="hide">
                    <div class="original-modal-body">
                        <div class="circle-loader"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- Modal -->
    <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModal"
        aria-hidden="true">
        <div class="modal-dialog" style="max-width: 400px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="confirmationModalTitle">Удалить?</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div id="confirmationModalContent" class="modal-body">
                    <div class="container-fluid">
                        Вы уверены? Вы не сможете отменить это действие! </div>
                </div>
                <div class="modal-footer clearfix">
                    <button id="confirmDeleteButton" type="button" class="btn btn-danger" data-bs-dismiss="modal"><svg
                            xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-trash-2 icon-16">
                            <polyline points="3 6 5 6 21 6"></polyline>
                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                            </path>
                            <line x1="10" y1="11" x2="10" y2="17"></line>
                            <line x1="14" y1="11" x2="14" y2="17"></line>
                        </svg> Удалить</button>
                    <button type="button" class="btn btn-default" data-bs-dismiss="modal"><svg
                            xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-x icon-16">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg> Отмена</button>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="./Календарь _ Task Manager_files/summernote.css">
    <script type="text/javascript" src="./Календарь _ Task Manager_files/summernote.min.js.Без названия"></script>
    <script type="text/javascript" src="./Календарь _ Task Manager_files/summernote-ru-RU.js.Без названия"></script>
    <div style="display: none;">
        <script type="text/javascript">
            feather.replace();

        </script>
    </div>


    <div class="modal-backdrop fade show"></div><input type="file" multiple="multiple" class="dz-hidden-input"
        style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;"><span
        role="status" aria-live="polite" class="select2-hidden-accessible"></span>
</body><grammarly-desktop-integration data-grammarly-shadow-root="true"></grammarly-desktop-integration>

</html>